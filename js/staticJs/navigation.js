console.log(`http://${location.hostname}`);
let functionNavigation = {
	clickPage: async function(self) {
		let state = { 'page_id': self.getAttribute('data-pageId'), 'page': self.getAttribute("data-page")};
		let title = '';
		let id = "";
		(self.getAttribute("data-id"))?id = `?id=${self.getAttribute("data-id")}`:"";
		let url = `http://${location.hostname}:9901/#/${self.getAttribute("data-page")}${id}`;
		let data = {
			page: self.getAttribute('data-page'),
			getAttributs:dataObject.getUrlGet(url)
		};
		window.history.pushState(state, title, url);
		console.log("tutttt");
		let html = await this.ajaxPage(data);
		if(html)this.loadingAsyncScripts(data.page,html);
		
	},
	clickPage2: async function(self) {
		console.log(self);
		let state = {'page_id': self.pageId, 'page': self.page};
		let title = '';
		let id = "";
		(self.id)?id = `?id=${self.id}`:"";

		let url = `http://${location.hostname}:9901/#/${self.page}${id}`;
		let data = {
			page: self.page,
			getAttributs:dataObject.getUrlGet(url)
		};
		window.history.pushState(state, title, url);
		let html = await this.ajaxPage(data);
		this.loadingAsyncScripts(data.page,html);
	},
	inLoadingPage: async function(data) {
		let html = await this.ajaxPage(data);
		if(html)this.loadingAsyncScripts(data.page,html);
		
	},
	loadingAsyncScripts: function(page,html){
		let eventScript = document.createElement("script");
		let main = document.getElementsByClassName("main")[0];
		let functionScript = document.createElement("script");
		functionScript.src = `functionsPage/${page}.js`;
		eventScript.src = `events/${page}.js`;
		main.innerHTML = html;
		main.appendChild(functionScript);
		main.appendChild(eventScript);
	},
	ajaxPage:async data => {
		let buffer = await fetch(`http://${location.hostname}:9901/${data.page}`,{
									method:"POST",
									body:JSON.stringify({data}),
									credentials: 'include',
									headers:{'Content-Type': 'application/json',
									'Access-Control-Allow-Origin':'true'},
								}).then(res=>res.text());
		try{
			buffer = JSON.parse(buffer);
			console.log(buffer);
			if(buffer.redirect)window.location.replace(`${buffer.redirect}`);
			return false;
		}catch (err){
			return buffer;	
		}
		
		
	}
};