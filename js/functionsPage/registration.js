var regHandlers = {
	data: {},
	clearBorder(){
		for (var i = 0; i < document.getElementsByTagName("input").length; i++) {
			document.getElementsByTagName("input")[i].style.border = "none";
		}
	},
	firstStep (){
		this.clearBorder;
		(cf.mail(document.getElementsByTagName("input")[0].value))?this.data.username = document.getElementsByTagName("input")[0].value:document.getElementsByTagName("input")[0].style.border = "1px solid red";
		(cf.password(document.getElementsByTagName("input")[1].value))?this.data.password = document.getElementsByTagName("input")[1].value:document.getElementsByTagName("input")[1].style.border = "1px solid red";
		console.log(this.data);
		console.log(Object.keys(this.data).length);
		if(Object.keys(this.data).length >= 2){
			document.getElementsByTagName("form")[0].style.display = "none";
			document.getElementsByTagName("form")[1].style.display = "";	
		}
	},
	async secondStep (){
		this.clearBorder;
		(cf.empty(document.getElementsByTagName("input")[4].value))?this.data.company_name = document.getElementsByTagName("input")[4].value:document.getElementsByTagName("input")[4].style.border = "1px solid red";
		(cf.empty(document.getElementsByTagName("input")[5].value))?this.data.adress = document.getElementsByTagName("input")[5].value:document.getElementsByTagName("input")[5].style.border = "1px solid red";
		(cf.empty(document.getElementsByTagName("input")[6].value))?this.data.phone = document.getElementsByTagName("input")[6].value:document.getElementsByTagName("input")[6].style.border = "1px solid red";
		console.log(this.data);
		if(Object.keys(this.data).length == 5){
			this.data.method = "SIGNUP";
			let buffer = await fetch(`/api/auth`,{
									method:"POST",
									headers:{'Content-Type': 'application/json',
									
									'Access-Control-Allow-Origin':'true'},
									body: JSON.stringify(this.data),
									credentials: 'include',
								}).then(res=>res.json());
			console.log(buffer);
			if(buffer.id){
				window.location.replace("/admin");
			}
		}
	},
	canselStep (){
		document.getElementsByTagName("form")[1].style.display = "none";
		document.getElementsByTagName("form")[0].style.display = "";
	}
}