var routes = require('express').Router,
subd = require('../models/subd');

routes.post("/",async (req,res)=>{
    switch(req.body.method){
        case "POST":
            if(
                !req.body.item_id || !req.body.background_id || 
                !req.body.avatar_id || !req.signedCookies['user_id'] ||
                !req.body.user_id
            ){
                res.status(418)
                .json({
                    message:"Not enough data"
                }).end();
            }else{
                let con = await subd.connect();
                let owner = (await con.query(`
                select company_id from user
                where id = ?
                `,[
                    req.body.user_id
                ]).catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    }).end();
                }));
                if(owner[0].length>0){
                    if(owner[0][0].company_id == req.signedCookies['user_id']){
                        let create_gift = (await con.query(`
                            INSERT INTO gifts SET
                            item_id = ?
                            background_id = ?,
                            avatar_id = ?,
                            reciever_id = ?,
                            activated = false,
                            sender_id = ?
                            `,[
                                req.body.item_id,
                                req.body.background_id,
                                req.body.avatar_id,
                                req.body.user_id,
                                req.signedCookies['user_id']
                            ]).catch(err=>{
                                res.status(500)
                                .json({
                                    message:err.sqlMessage
                                }).end();
                            }));
                            res.status(200)
                            .json({
                                gift_id:create_gift[0].insertId
                            }).end();
                    }else{
                        res.status(403)
                        .json({
                            message:"This user isnot in your company"
                        }).end();
                    }
                }else{
                    res.status(418)
                    .json({
                        message:"Wrong user id"
                    }).end();
                }
                
                con.end();
            }
            break;
        case "GET":
            if(!req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"need to auth"
                }).end();
            }else{
                let con = await subd.connect();
                let get_gifts = (await con.query(`
                SELECT id,item_id,background_id,avatar_id from gifts
                WHERE reciever_id = ? and activated=false
                `,[
                    req.signedCookies['user_id']
                ]).catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    }).end();
                }));
                con.end();
            }
            break;
        case "ACTIVATE":
            if(
                !req.signedCookies['user_id'] || !req.body.what || !req.body.what_id || !req.body.gift_id
            ){
                res.status(418)
                .json({
                    message:"Not enough data"
                })
            }else{
                let con = await subd.connect();
                let reciever = (await con.query(`
                select * from gifts
                where id = ?
                `,[
                    req.signedCookies['user_id']
                ]).catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    }).emd();
                }))
                if(reciever[0].length>0){
                    if(reciever[0][0].reciever_id == req.signedCookies['user_id']){
                        switch(req.body.what){
                            case "item":
                                let add_item = (await con.query(`
                                    update gifts set
                                    item_id = ?,
                                    background_id = 0,
                                    avatar_id = 0,
                                    activated = true
                                    where id = ?
                                `,[
                                    req.body.what_id,
                                    req.body.gift_id
                                ]).catch(err=>{
                                    res.status(500)
                                    .json({
                                        message:err.sqlMessage
                                    }).end();
                                }));
                                res.status(200)
                                .json({
                                    message:"ITEM Activated:"+add_item[0].affectedRows
                                }).end();
                                break;
                            case "background":
                                let add_bg = (await con.query(`
                                        update gifts set
                                        item_id = 0,
                                        background_id = ?,
                                        avatar_id = 0,
                                        activated = true
                                        where id = ?
                                    `,[
                                        req.body.what_id,
                                        req.body.gift_id
                                    ]).catch(err=>{
                                        res.status(500)
                                        .json({
                                            message:err.sqlMessage
                                        }).end();
                                    }));
                                    res.status(200)
                                    .json({
                                        message:"BG Activated:"+add_bg[0].affectedRows
                                    }).end();
                                break;
                            case "avatar":
                                let add_avatar = (await con.query(`
                                        update gifts set
                                        item_id = 0,
                                        background_id = 0,
                                        avatar_id = ?,
                                        activated = true
                                        where id = ?
                                    `,[
                                        req.body.what_id,
                                        req.body.gift_id
                                    ]).catch(err=>{
                                        res.status(500)
                                        .json({
                                            message:err.sqlMessage
                                        }).end();
                                    }));
                                    res.status(200)
                                    .json({
                                        message:"AVATAR Activated:"+add_avatar[0].affectedRows
                                    }).end();
                                break;
                            default:
                                res.status(418)
                                .json({
                                    message:"What the fuck?"
                                }).end();
                        }

                    }else{
                        res.status(418)
                        .json({
                            message:"Its not ur gift"
                        }).end();
                    }
                }else{
                    res.status(418)
                    .json({
                        message:"u cant activate gift"
                    })
                }
                con.end();
            }
            let con = await subd.connect();

            con.end();
            break;
        
        default:
            res.status(418)
            .json({
                message:"bad method"
            }).end();
    }

})

module.exports = routes;