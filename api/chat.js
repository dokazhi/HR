var routes = require('express').Router(),
subd = require('../models/subd');

routes.ws("/",async (ws,req)=>{
    if(!req.signedCookies['user_id']){
        console.log("have not cookie")
        ws.close();
    }else{
        console.log("have a cookie")
        ws.socket_id = req.signedCookies['user_id'];
        online.push(ws);
        // ws.send(JSON.stringify({type:0,message:req.signedCookies['user_id']}));
        ws.on('message',async msg=>{
            let data = JSON.parse(msg);
            let con = await subd.connect();
            let insert_message = (await con.query(`INSERT INTO message SET
            message = ?,
            sender_id = ?,
            receiver_id =?
            `,[
                data.message,
                req.signedCookies['user_id'],
                data.user_id
            ]).catch(err=>{console.log(err.sqlMessage);return false;}))
            if(insert_message!=false){
                console.log("got enough")
            }
            con.end();
            
            for(let i=0;i<online.length;i++){
                if(online[i].socket_id==data.user_id){
                    online[i].send(JSON.stringify({
                        type:0,
                        message:data.message
                    }));
                }
            }
        });
        console.log("who go in:",ws.socket_id);
        ws.on('close',msg=>{
            online.splice(online.indexOf(ws),1)
            console.log("Who left is: ",ws.socket_id);
        })
    }
})
routes.post("/", async (req,res)=>{
    if(!(req.body.user_id!=null)||!req.body.message ||!req.signedCookies['user_id']){
        res.status(418)
        .json({
            message:"Not enough data"
        })
    }else{
        let con = await subd.connect();
            let insert_message = (await con.query(`INSERT INTO message SET
            message = ?,
            sender_id = ?,
            receiver_id =?
            `,[
                req.body.message,
                req.signedCookies['user_id'],
                req.body.user_id
            ]).catch(err=>{console.log(err.sqlMessage);return false;}))
            
            
            
        for(let i =0;i<online.length;i++){
            if(online[i].socket_id==req.body.user_id){
                online[i].send(JSON.stringify({type:0,message:req.body.message}));
                
            }
            
        }
        if(insert_message!=false){
            res.status(200)
                .json({
                    message:"Sended"
            })
        }else{
            res.status(500)
            .json({
                message:"SQL ERROR"
            })
        }
        con.end();
        
        
    }
});
routes.post("/messages",async (req,res)=>{
    if(!(req.body.user_id!=null)||!req.signedCookies['user_id']){
        res.status(418)
        .json({
            message:"Не хватает данных"
        });
    }else{
        
        let con = await subd.connect();
        let get_messages = (await con.query(`
        SELECT * FROM message WHERE
        sender_id = ? AND receiver_id = ?
        `,[
            req.signedCookies['user_id'],
            req.body.user_id
        ]).catch(err=>{console.log(err.sqlMessage); return false;}))
        if(get_messages!=false){
            res.status(200)
            .json(get_messages[0]);
        }else{
            res.status(500)
            .json({
                message:"Something wrong with SQL"
            })
        }
        con.end();
    }
})
module.exports = routes;