var routes = require('express').Router(),
subd = require('../models/subd');

routes.post("/",async (req,res)=>{
    switch(req.body.method){
        case "POST":
            if(!req.body.name || !req.body.description || !req.body.spot_id || !req.signedCookies['user_id'] || !req.body.status){
                res.status(418)
                .json({
                    message:"Not enough data"
                })
            }else{
                let con = await subd.connect();
                let add_skill = (await con.query(`
                INSERT INTO skills SET
                name = ?,
                description = ?,
                spot_id = ?,
                user_id = ?,
                status = ?
                `,[
                    req.body.name,
                    req.body.description,
                    req.body.spot_id,
                    req.signedCookies['user_id'],
                    req.body.status
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(201)
                .json({
                    id:add_skill[0].insertId
                })
                con.end();
            }
            break;
        case "GET":
            if(!req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"not authorized"
                })
            }else{
                let con = await subd.connect();
                let get_all = (await con.query(`
                select id, name, spot_id,description,status from skills
                where deleted=false and user_id = ?
                `,[
                    req.signedCookies['user_id']
                ]).catch(err=>{
                    res.status(500)
                    json({
                        message:err.sqlMessage
                    })
                }));
                if(get_all[0].length>0){
                    res.status(200)
                    .json({
                        body:get_all[0]
                    })
                }else{
                    res.status(200)
                    .json({
                        body:"empty"
                    })
                }
                con.end();
            }
            break;
        case "GETBYSTATUS":
            if(!req.body.staus || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else{
                let con = await subd.connect();
                let get_by_status = (await con.query(`SELECT id,name,description,spot_id,status FROM skills
                WHERE status = ? AND deleted = false
                
                `,[req.body.status])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    body:get_by_status[0]
                })
                con.end();
            }
            break;

        case "GET_SPOT":
            if(!(req.body.spot_id!=null)){
                res.status(418)
                .json({
                    message:"No no no"
                })
            }else{
                let con = await subd.connect();
                let spot_skill =  (await con.query(`
                SELECT * FROM skills where spot_id = ?
                `,[
                    req.body.spot_id
                ]).catch(err=>{console.log(err.sqlMessage);return false;}));
                if(spot_skill!=false){
                    res.status(200)
                    .json(spot_skill[0])
                }else{
                    res.status(500)
                    .json({
                        message:"Nooo"
                    })
                }
                con.end();
            }
            break;

        case "GETBYSPOT":
            if(!(req.body.spot_id!=null) || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else{
                let con = await subd.connect();
                let get_by_spot = (await con.query(`
                SELECT id,name,description,status from skills
                WHERE spot_id = ? AND user_id = ? AND deleted = false
                `,[
                    req.body.spot_id,
                    req.signedCookies['user_id']
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                if(get_by_spot[0].length>0){
                    res.status(200)
                    .json({
                        body:get_by_spot[0]
                    })
                }else{
                    res.status(200)
                    .json({
                        body:"empty"
                    })
                }
                con.end();
            }
            break;
        case "PATCH":
            if(!req.body.name || !req.body.id || !req.body.spot_id || !req.body.description || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else{
                let con = await subd.connect();
                let upd_skill = (await con.query(`
                UPDATE skills SET 
                name = ?,
                spot_id = ?,
                description = ?
                WHERE id = ? and user_id = ? AND deleted = false

                `,[
                    req.body.name,
                    req.body.spot_id,
                    req.body.description,
                    req.body.id,
                    req.signedCookies['user_id']
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    message:'updated'+upd_skills[0].affectedRows
                })
                con.end();
            }
            break;
        case "DELETE":
            if(!req.body.id || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else{
                let con = await subd.connect();
                let delete_skill = (await con.query(`
                DELETE FROM skills WHERE id = ? AND user_id = ?
                `,[
                    req.body.id,
                    req.signedCookies['user_id']
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    message:"deleted skills"+delete_skill[0].affectedRows
                })
                con.end();
            }
            break;
        default:
            res.status(418)
            .json({
                message:"WRONG METHOD"
            });
            break;
    }
});
module.exports = routes;