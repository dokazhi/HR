var routes = require('express').Router(),
subd = require('../models/subd');
routes.post('/',async (req,res)=>{
    switch(req.body.method){
        case "POST":
            if(
                !req.body.achievement_id || !req.body.user_id ||
                !req.signedCookies['user_id']
            ){
                res.status(418)
                .json({
                    message:'not enough data'
                })
            }else{
                let con = await subd.connect();
                let select_company = (await con.query(`
                    select company_id from user where id =?
                `,[
                    req.body.user_id
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                if(select_company[0].length>0){    
                    if(select_company[0][0].company_id == req.signedCookies['user_id']){
                        let add_achiev = (await con.query(`
                            INSERT INTO user_achievements SET
                            user_id = ?,
                            achievement_id = ?
                            
                        `,[
                            req.body.user_id,
                            req.body.achievement_id
                        ])
                        .catch(err=>{
                            res.status(500)
                            .json({
                                message:err.sqlMessage
                            })
                        }));
                        res.status(200)
                        .json({
                            id:add_achiev[0].insertId
                        })
                    }else{
                        res.status(418)
                        .json({
                            message:"U just cant"
                        })
                    }
                }else{
                    res.status(418)
                    .json({
                        message:"nothing found"
                    })
                }
                con.end();
            }
            break;
        case "GET":
            if(req.body.user_id){
                let con = await subd.connect();
                let get_achievs = (await con.query(`
                SELECT a.id, a.name, a.category_id, a.description, a.point, ua.date_recieved 
                FROM achievements AS a INNER JOIN user_achievements AS ua ON a.id=ua.achievement_id
                WHERE ua.user_id = ?
                `,[
                    req.body.user_id
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                if(get_achievs[0].length>0){
                    res.status(200)
                    .json({
                        body:get_achievs[0]
                    })
                }else{
                    res.status(418)
                    .json({
                        message:"nothing found"
                    })
                }
                con.end();
            }else{
                res.status(418)
                .json({
                    message:'not enough data'
                })
            }
            break;
        
        
        default:
            res.status(418)
            .json({
                message:"bad method"
            });
            break;
    
   }

});
module.exports = routes;