var routes = require('express').Router(),
subd = require('../models/subd');
routes.post('/',async (req,res)=>{
    switch(req.body.method){
        case "POST":
            if(!req.body.skill_id || !req.body.percent || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else{
                let con = await subd.connect();
                let ins_skill = (await con.query(`
                    INSERT INTO user_skills SET
                    skill_id = ?,
                    user_id = ?,
                    percent = ?,
                    activated = false
                `,[
                    req.body.skill_id,
                    req.signedCookies['user_id'],
                    req.body.percent
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }))
                con.end();
                res.status(201)
                .json({
                    id:ins_skill[0].insertId
                })
            }
            break;
        case "GET":
            if(!req.body.user_id){
                res.status(500)
                .json({
                    message:"which one u need?"
                })
            }else{
                let con = await subd.connect();
                let skills = (await con.query(`
                SELECT s.id, s.name, s.description, s.spot_id, s.item_type, us.percent
                    FROM user_skills as us INNER JOIN skills as s ON s.id = us.skill_id
                    WHERE us.user_id = ?
                `,[
                    req.body.user_id
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }))
                con.end();
                if(skills[0].length>0){
                    res.status(200)
                    .json({
                        body:skills[0]
                    })
                }else{
                    res.status(500)
                    .json({
                        message:"no skills"
                    })
                }
                
            }
            break;
        // case "PATCH":
        //     break;
        // case "DELETE":
        //     break;
        case "CONFIRM":
            if(!req.body.user_skill_id || !req.signedCookies['user_id'] || !req.body.percent){
                res.status(418)
                .json({
                    message:"what to confirm?"
                })
            }else{
                let con = await subd.connect();
                
                let sel_hr = (await con.query(`
                    select s.user_id from skills as s INNER JOIN user_skills as us ON s.id=us.skill_id WHERE us.id = ?  
                `,[
                    req.body.user_skill_id
                ])
                .catch(async err=>{
                
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                if(sel_hr[0].length>0){
                    if(set_hr[0][0].user_id == req.sigendCookies['user_id']){
                        let confrim_skill = (await con.query(`
                            UPDATE user_skills SET percent = ?, activated = true
                            WHERE id = ?
                            `,[
                                req.body.percent,
                                req.body.user_skill_id
                            ])
                            .catch(err=>{
                                res.status(500)
                                .json({
                                    message:err.sqlMessage
                                })
                            }));
                        if(confirm_skill[0].affectedRows>0){
                            res.status(200)
                            .json({
                                message:"affected rows"+confirm_skill[0].affectedRows
                            })
                        }else{
                            res.status(418)
                            .json({
                                message:"Not affected rows"
                            })
                        }
                    }else{
                        res.status(418)
                        .json({
                            message:"U cant confirm this skill"
                        })
                    }
                }else{
                    res.status(418)
                    .json({
                        message:"sam hz, no eto ne kanaet"
                    })
                }
                
                con.end();
            }
            break;
    }
});
module.exports = routes;