var routes = require('express').Router(),
subd = require('../models/subd');

routes.post("/",async (req,res)=>{
    switch(req.body.method){
        case "POST":
            if(
                !req.body.user_id || !req.body.ba_id ||
                !req.signedCookies['user_id']
            ){
                res.status(418)
                .json({
                    message:"Not enough adta"
                })
            }else{
                let con = await subd.connect();
                let owner = (await con.query(`
                select ba.owner_id as owner_id from background_avatar as ba 
                INNER JOIN  user_ba as uba ON ba.id = uba.id_ba
                where uba.id_ba = ?
                `,[
                    req.body.ba_id
                ])
                .catch(err=>{
                    res.status(500
                    .json({
                        message:err.sqlMessage
                    }))
                }));
                if(owner[0].length>0){
                    if(owner[0][0].owner_id == req.signedCookies['user_id']){
                        let add = (await con.query(`
                            INSERT INTO user_ba SET id_ba = ?, user_id = ?
                        `,[
                            req.body.ba_id,
                            req.body.user_id
                        ]).catch(err=>{
                            res.status(500)
                            .json({
                                message:err.sqlMessage
                            })
                        })
                        );
                        res.status(201)
                        .json({
                            id:add[0].insertId
                        })
                    }
                }else{
                    res.status(418)
                    .json({
                        message:"Not found"
                    })
                }
                con.end();
            }
            break;
        case "GET":
            if(!req.body.user_id){
                res.status(418)
                .json({
                    message:"Which one data i want"
                })
            }else{
                let con = await subd.connect();
                let get_ba = (await con.query(`
                    select ba.id, ba.path, ba.type from background_avatar as ba
                    INNER JOIN user_ba as uba ON ba.id = uba.id_ba
                    where uba.user_id = ?
                `,[
                    req.body.user_id
                ]).catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    body:get_ba[0]
                })
                con.end();
            }
            break;
        case "DELETE":
            if(!req.body.ba_id || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"Bad data"
                })
            }else{
                let con = await subd.connect();
                let owner = (await con.query(`
                    select user_id from user_ba where id = ?
                `,[
                    req.body.ba_id
                ]).catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }))
                if(owner[0].length>0){
                    if(owner[0][0].user_id == req.signedCookies['user_id']){
                        let del_ba = (await con.query(`
                            DELETE FROM user_ba where id = ?
                        `,[
                            req.body.ba_id
                        ]).catch(err=>{
                            res.status(500)
                            .json({
                                message:err.sqlMessage

                            })
                        }));
                        if(del_ba[0].affectedRows>0){
                            res.status(200)
                            .json({
                                message:"deleted"
                            })
                        }else{
                            res.status(418)
                            .json({
                                message:"not deleted"
                            })
                        }
                    }else{
                        res.status(403)
                        .json({
                            message:"u cant delete this"
                        })
                    }
                }else{
                    res.status(418)
                    .json({
                        message:"not found"
                    })
                }
                
                con.end();
            }
            break;
        default:
            res.status(418)
            .json({
                message:"BAD method"
            });
            break;
    }
})

module.exports = routes;