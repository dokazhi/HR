var routes = require('express').Router(),
fs = require('fs'),
subd = require('../models/subd');

routes.post('/',async (req,res)=>{
    console.log(req.body);
    switch(req.body.method){
        case "POST":
            if(
                !req.body.name || !req.body.description ||
                !req.body.category_id || !req.body.points ||
                !req.body.date_achieves || !req.signedCookies['user_id'] || !req.files.picture
            ){
                console.log("NOT ENOUGH DATA")
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else if(!(
                    req.files.picture.mimetype!=='image/png' || 
                    req.files.picture.mimetype !== 'image/jpeg' 
                    )){
                        console.log("BAD MIME TYPE")
                res.status(418)
                .json({
                    message:"wrong file type"
                })
            }else{
                console.log("RUN")
                let con = await subd.connect();
                await con.query(`START TRANSACTION`);
                let add_achiev = (await con.query(`
                    INSERT INTO achievements SET
                    name = ?,
                    description = ?,
                    category_id = ?,
                    point = ?,
                    date_achieves = ?,
                    user_id = ?,
                    path = ''
                `,[
                    req.body.name,
                    req.body.description,
                    req.body.category_id,
                    req.body.points,
                    req.body.date_achieves,
                    req.signedCookies['user_id']
                ])
                .catch(async err=>{
                    console.log(err);
                        await con.query("ROLLBACK");
                    }));
                    let ext = req.files.picture.name.substr(req.files.picture.name.length-4)
                    let path = `/uploads/achievements/achieve${add_achiev[0].insertId}${ext}`;
                    await fs.writeFileSync("/var/www/projects/team.aida.market/node_hr/public/uploads/achievements/achieve"+add_achiev[0].insertId+ext,req.files.picture.data,"binary");
                        
                            let upd = (await con.query(`update achievements set path=? where id =?`,[
                                path,
                                add_achiev[0].insertId
                            ])
                            .catch(async err=>{
                                await con.query("ROLLBACK");
                                res.status(500)
                                .json({
                                    message:err.sqlMessage
                                })
                            }));
                            await con.query("COMMIT");
                            await res.status(200)
                            .json({
                                id:add_achiev[0].insertId,
                                path:path
                            })
                
                con.end;
            }
            break;
        case "GET":
            if(!req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"Just cant sorry"
                })
            }else{
                let con = await subd.connect();
                let achieves = (await con.query(`
                    SELECT id,name,description,path,category_id,point,date_achieves from achievements
                    WHERE user_id = ? AND deleted=false
                `,[req.signedCookies['user_id']])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    body:achieves[0]
                })
                con.end();
            }
            break;
        case "GETBYSPOT":
            if(!(req.body.spot_id!=null)){
                res.status(418)
                .json({
                    message:"what to get?"
                })
            }else{
                
                let con = await subd.connect();
                let achiev = (await con.query(`
                SELECT * from achievements where category_id = ?
                `,[
                    req.body.spot_id
                ]).catch(err=>{console.log(err.sqlMessage);return false;}))
                if(achiev!=false){
                    res.status(200)
                    .json(
                        achiev[0]
                    )
                }else{
                    res.status(500)
                    .json({
                        message:"something goes wrong"
                    })
                }
                con.end();
            }
            break;
        case "PATCH":
            if(
                !req.body.id || !req.body.description || !req.body.name ||
                !req.body.category_id || !req.body.points || !req.body.date_achieves ||
                !req.signedCookies['user_id']
            ){
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else{
                let con = await subd.connect();
                let patch_achiev = (await con.query(`
                UPDATE achievements SET
                name = ?,
                description = ?,
                point = ?,
                category_id = ?,
                date_achieves = ?
                WHERE id = ? AND user_id = ?
                `,[
                    req.body.name,
                    req.body.description,
                    req.body.points,
                    req.body.category_id,
                    req.body.id,
                    req.signedCookies['user_id']
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }))
                con.end();
                res.status(200)
                .json({
                    message:"affected rows:"+patch_achiev[0].affectedRows
                })

            }
            break;
        case "DELETE":
            if(!req.body.id || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"Not enough data"
                });
            }else{
                let con = await subd.connect();
                let del_achiev = (await con.query(`
                DELETE from achievements
                where id = ? AND user_id = ?
                `,[
                    req.body.id,
                    req.signedCookies['user_id']
                ])
                    .catch(err=>{
                        res.status(500)
                        .json({
                            message:err.sqlMessage
                        })
                    }));
                res.status(200)
                .json({
                    message:'affected rows'+del_achiev[0].affectedRows
                })
                con.end();
            }
            break;

        default:
            res.status(418)
            .json({
                message:"Invalid method or not provided"
            })
    }

});


module.exports = routes;