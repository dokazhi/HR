var routes = require('express').Router(),
subd = require('../models/subd');
routes.post("/",async (req,res)=>{
    switch(req.body.method){
        case "POST":
            if(!req.body.pskill_id || !req.body.user_id || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else{
                let con = await subd.connect();
                let select_owner = (await con.query(`
                select company_id from user where id = ?
                `,[
                    req.body.user_id
                ]));
                if(select_owner[0].length>0){
                    if(select_owner[0][0].company_id == req.signedCookies['user_id']){
                        let add_psk_user = (await con.query(`
                        INSERT INTO pskills_user SET
                        pskill_id = ?,
                        user_id =?
                        `,[
                            req.body.pskill_id,
                            req.body.user_id
                        ])
                        .catch(err=>{
                            res.status(500)
                            .json({
                                message:err.sqlMessage
                            })
                        }));
                        if(add_psk_user){
                            res.status(200)
                            .json({
                                id:add_psk_user[0].insertId
                            })
                        }
                    }else{
                        res.status(403)
                        .json({
                            message:"not your employee"
                        })
                    }
                }else{
                    res.status(418)
                    .json({
                        message:"Wrong user id"
                    })
                }
                
                con.end();
            }
            
            break;
        case "GET":
            if(!req.body.user_id){
                res.status(418)
                .json({
                    message:"not id"
                })
            }else{
                let con = await subd.connect();
                let get_pskills = (await con.query(`
                select ps.id,ps.name,ps.description from personal_skills as ps
                INNER JOIN pskills_user as pu ON ps.id = pu.pskill_id
                WHERE pu.user_id = ?
                `,[
                    req.body.user_id
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    body:get_pskills[0]
                })
                con.end();
            }
            break;
        case "DELETE":
            if(!req.body.id || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"Sorry but no"
                })
            }else{
                let con = await subd.connect();
                let owner = (await con.query(`SELECT user_id FROM pskills_user where id = ?`,[req.body.id])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                if(owner[0].length>0){
                    if(owner[0][0].user_id == req.signedCookies['user_id']){
                        let del_pskl_user = (await con.query(`
                            DELETE FROM pskills_user WHERE id =?
                        `,[
                            req.body.id
                        ])
                        .catch(err=>{
                            res.status(500)
                            .json({
                                message:err.sqlMessage
                            })
                        }))
                        res.status(200)
                        .json({
                            message:"deleted:"+del_pskl_user[0].affectedRows
                        })
                    }else{
                        res.status(418)
                        .json({
                            message:"U cant delete this"
                        })
                    }
                }else{
                    res.status(418)
                    .json({
                        message:'wrong id'
                    })
                }
                
                con.end();
            }
            break;
        default:
            res.status(418)
            .json({
                message:"Bad method"
            });
            break;
    }
})
module.exports = routes;