var routes = require('express').Router(),
key = "hervam"
crypto = require('crypto'),
subd = require('../models/subd');



routes.post('/',async (req,res)=>{
    console.log(req.body);
    switch(req.body.method){
        case "LOGOUT":
            res.clearCookie("user_id",{ path: '/', domain:"demo.aida.market" });
            res.status(200)
            .json({
                message:"Succesfully logged out"
            });
            break;
        case "SIGNIN":
        if(req.signedCookies['user_id']){
            res.status(200)
            .json({
                message:"U cant signup, u r logged in"
            })

        }else{
            if(!req.body.username || !req.body.password || !(req.body.state!=null)){
                res.status(403)
                .json({
                    message:"Not enough data"
                })
            }else{
                let con = await subd.connect();
                let select_user = (await con.query(`
                select * from user where username = ? and activated=true
                `,[
                    req.body.username
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                if(select_user[0].length>0){
                    let hashed_password = crypto.createHmac('sha1',key).update(req.body.password).digest('hex');
                    if (select_user[0][0].password = hashed_password){
                        if(req.body.state==true){
                            res.cookie('user_id',select_user[0][0].id,{
                                maxAge:new Date(Date.now() + 365*24*60*60*1000),//2067 at 2018.01.06
                                httpOnly:true,
                                signed:true
                            });
                        }else{
                            res.cookie('user_id',select_user[0][0].id,{
                               
                                httpOnly:true,
                                signed:true
                            });
                        }


                            res.status(200)
                            .json({
                                success:true,
                                company_id:select_user[0][0].company_id,
                                message:"loged in"
                            });
                        // }else{
                        //     res.cookie('user_id',select_user[0][0].id,{
                                
                        //         httpOnly:true,
                        //         signed:true
                        //     })
                        //     res.status(200)
                        //     .json({
                        //         success:true,
                        //         company_id:select_user[0][0].company_id,
                        //         message:"loged in"
                        //     });
                        // }
                    }else{
                        res.status(403)
                        .json({
                            message:'wrong password'
                        })
                    }
                }else{
                    res.status(403)
                    .json({
                        message:"this user is not exists"
                    })
                }
                con.end();
            }
            }
            break;
        case "SIGNUP":
        console.log(req.body);
            if(req.signedCookies['user_id']){
                
                res.status(200)
                .json({
                    message:"U cant signup, u r logged in"
                })

            }else{

            if(!req.body.username || !req.body.password || !req.body.company_name || !req.body.adress || !req.body.phone){
                res.status(403)
                .json({
                    message:"Soory no"
                })
            }else{
                let con = await subd.connect();
                await con.query("START TRANSACTION");
                let select_user = (await con.query(`
                SELECT * FROM user WHERE username= ?
                `,[
                    req.body.username
                ])
                .catch(async err=>{
                    await con.query("ROLLBACK");
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    }).end();
                }));
                if(select_user[0].length>0){
                    await con.query("ROLLBACK");
                    res.status(418)
                    .json({
                        message:"alllready exists"
                    })
                }else{
                    let hashed_password = crypto.createHmac('sha1',key).update(req.body.password).digest('hex');
                    let new_acc = (await con.query(`
                    INSERT INTO user SET username = ?,password = ?,invited=false,activated=true,company_id=0
                    `,[
                        req.body.username,
                        hashed_password
                    ])
                    .catch(async err=>{
                        await con.query("ROLLBACK");
                        res.status(500)
                        .json({
                            message:err.sqlMessage
                        })
                    }));
                    let new_acc_data = (await con.query(`
                    INSERT INTO user_data SET
                    type = 0,
                    user_id = ?,
                    data = ?

                    `,[
                        new_acc[0].insertId,
                        JSON.stringify({
                            company_name : req.body.company_name,
                            company_adress : req.body.adress,
                            company_phone : req.body.phone
                        })
                    ])
                    .catch(async err=>{
                        await con.query("ROLLBACK");
                        res.status(500)
                        .json({
                            message:err.sqlMessage
                        })
                    }))
                    res.cookie('user_id',new_acc[0].insertId,{
                        maxAge:365*24*60*60,
                        httpOnly:true,
                        signed:true
                    })
                    await con.query("COMMIT");
                    
                    res.status(201)
                    .json({
                        id:new_acc[0].insertId
                    })
                    con.end();
                }
            }
        }
            break;
        case "ACTIVATE":
        console.log(">>>>>>>>>>>>>IN ACTIVATE<<<<<<<<<<<<<<<<");
            if(!req.body.data || !req.body.token){
                res.status(418)
                .json({
                    message:"ne hvataet dannih"
                });
            }else{
                let con = await subd.connect();
                let get_token = (await con.query(`
                    select * from invite_tokens where token = ?
                `,[
                    req.body.token
                ]).catch(err=>{return false;}))
                // let create_user = con.query(``)
                if(get_token!=false){
                    if(get_token[0].length>0){
                        let del_token = (await con.query(`delete from invite_tokens where id = ?`,[get_token[0][0].id]).catch(err=>{
                            console.log(err);
                            return false;
                        }));
                        if(del_token!=false){
                            let hasehd_password = crypto.createHmac('sha1',key).update(req.body.data.password).digest('hex');
                            let insert_user = (await con.query(`
                            INSERT INTO user SET
                            username = ?,
                            password = ?,
                            company_id = ?,
                            activated=true,
                            invited=true,
                            spot_id = ?
                            `,[
                                get_token[0][0].email,
                                hasehd_password,
                                get_token[0][0].owner_id,
                                get_token[0][0].spot_id
                            ]).catch(err=>{return false;}));
                            if(insert_user!=false){
                                let insert_data = (await con.query(`
                                INSERT INTO user_data SET
                                type=true,
                                data = ?,
                                user_id=?
                                
                                `,[
                                    JSON.stringify(
                                        {
                                            birthday:req.body.data.date,
                                            male:req.body.data.male,
                                            name:req.body.data.name,
                                            surname:req.body.data.surname,
                                            phone:req.body.data.phone,
                                            telegram:req.body.data.telega

                                        }

                                    ),
                                    insert_user[0].insertId

                                ]).catch(err=>{return false}));
                                if(insert_data!=false){
                                    res.cookie('user_id',insert_user[0].insertId,{
                                        maxAge:365*24*60*60,
                                        httpOnly:true,
                                        signed:true
                                    });
                                    res.status(201)
                                    .json({
                                        success:true,
                                        message:"Created his id"+insert_user[0].insertId
                                    })
                                }else{
                                    res.status(500)
                                    .json({
                                        message:"SOmething wrong with data adding"
                                    })
                                }
                            }else{
                                res.status(500)
                                .json({
                                    message:"error on creation"
                                })
                            }
                        }else{
                            res.status(500)
                            .json({
                                message:"Got an error"
                            })
                        }
                    }else{
                        res.status(418)
                        .json({
                            message:"Bad token"
                        })
                    }
                }else{
                    res.status(500)
                    .json({
                        message:"Got error when getting token"
                    })
                }
                con.end();
            }
            break;
        default:
            res.status(418)
            .json({
                message:"Dont"
            })
            break;
    }


})

module.exports = routes;