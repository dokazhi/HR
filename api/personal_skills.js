var routes = require('express').Router(),
subd = require('../models/subd');

routes.post('/',async (req,res)=>{
    switch(req.body.method){
        case "POST":
            if(!req.body.name || !req.body.description || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else{
                let con = await subd.connect();
                let new_pskill = (await con.query(`
                INSERT INTO personal_skills SET
                name = ?,
                description = ?,
                user_id = ?
                `,[
                    req.body.name,
                    req.body.description,
                    req.signedCookies['user_id']
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    id:new_pskill[0].insertId
                })
                con.end();
            }
            break;
        case "GET":
            let con = await subd.connect();
            let pskills = (await con.query(`select id,name,description,user_id from personal_skills`)
            .catch(err=>{
                res.status(500)
                .json({
                    message:err.sqlMessage
                });
            }));
            
                res.status(200)
                .json(
                    pskills[0]                )
            
            con.end();
            break;
        
        case "PATCH":
            if(!req.body.name || !req.body.description || !req.body.id){
                res.status(418)
                .json({
                    message:'Not enough data'
                })
            }else{
                let con = await subd.connect();
                let owner = (await con.query(`
                select user_id from personal_skills where id = ?
                `,[
                    req.body.id
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                if(owner[0][0].user_id == req.signedCookies['user_id']){
                    let update_pskill = (await con.query(`
                    update personal_skills set
                    name = ?,
                    description = ?
                    where id = ?

                    `,[
                        req.body.name,
                        req.body.description,
                        req.body.id
                    ]).catch(err=>{
                        res.status(500)
                        .json({
                            message:err.sqlMessage
                        });
                    }));
                }else{
                    res.status(403)
                    .json({
                        message:"It is not yours"
                    })
                }
                
                con.end();
            }
            break;
        case "DELETE":
            if(!req.body.id || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"Not enough data"
                })
            }else{
                let con = await subd.connect();
                let sel_user = (await con.query(`select user_id from personal_skills where id =?`,[req.body.id])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                if(sel_user[0][0].user_id == req.signedCookies['user_ud']){
                    let mark_deleted = (await con.query(`UPDATE personal_skills SET
                    deleted=true
                    WHERE id = ?
                    `,[req.body.id])
                    .catch(err=>{
                        res.status(500)
                        .json({
                            message:err.sqlMessage
                        })
                    }))
                }else{
                    res.status(403)
                    .json({
                        message:'u cant delete this'
                    })
                }
                
                con.end();
            }
            break;
        default:
            res.status(418)
            .json({
                message:"Bad method"
            });
            break;
    }
})


module.exports = routes;