var routes = require('express').Router(),
fs = require('fs'),
subd = require('../models/subd');

routes.post('/',async (req,res)=>{
    switch (req.body.method){
        case "POST":
        console.log(req.body);
        console.log(req.files);
            if(!req.body.name || !req.body.description || 
                !(req.body.spot_id!=null) || !req.files.picture ||
                !(req.body.category!=null)||
                !(req.body.item_type!=null) || !req.signedCookies['user_id']
            ){
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else if(!(req.files.picture.mimetype!=='image/png' || req.files.picture.mimetype !== 'image/jpeg')){
                res.status(418)
                .json({
                    message:'wrong file type'
                });

            }else{
                let con = await subd.connect();
                
                
                //fs.writeFileSync(__dirname+"/public/uploads/"+req.body.register_number+req.files.maintenance.name.substr(req.files.maintenance.name.length-4),req.files.maintenance.data,"binary");
                //path = "/api/uploads/"+req.body.register_number+req.files.maintenance.name.substr(req.files.maintenance.name.length-4);
                let add_item = (await con.query(`
                    insert into items set
                    name = ?,
                    description = ?,
                    spot_id = ?,
                    item_type = ?,
                    user_id = ?,
                    category = ?
                `,[
                    req.body.name,
                    req.body.description,
                    req.body.spot_id,
                    req.body.item_type,
                    req.signedCookies['user_id'],
                    req.body.category
                ]).catch(async err=>{
                    
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })

                }));
                let ext = req.files.picture.name.substr(req.files.picture.name.length-4)
                let path = `/uploads/items/item${add_item[0].insertId}${ext}`;
                await fs.writeFileSync("/var/www/projects/team.aida.market/node_hr/public/uploads/items/item"+add_item[0].insertId+ext,req.files.picture.data,"binary")
                   
                        let upd = (await con.query(`update items set img_path=? where id =?`,[
                            path,
                            add_item[0].insertId
                        ])
                        .catch(async err=>{
                            console.log("erros is"+err)
                            res.status(500)
                            .json({
                                message:err.sqlMessage
                            })
                        }));
                        if(upd[0].affectedRows>0){
                            res.status(200)
                            .json({
                                id:add_item[0].insertId,
                                path:path
                            })
                        }
                    

                
                con.end();
            }
            break;
        case "GET":
            if(!req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"cant get"
                })
            }else{
                let con = await subd.connect();
                let get_items = (await con.query(`
                select * from items where user_id = ?
                `,[
                    req.signedCookies['user_id']
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    body:get_items[0]
                })
                con.end();
            }
            break;
        case "GETBYSPOT":
            if(!(req.body.spot_id!=null)){
                res.status(418)
                .json({
                    message:"not id"
                })
            }else{
                let con = await subd.connect();
                let items = (await con.query(`
                SELECT * from items where spot_id = ?
                `,[req.body.spot_id]).catch(err=>{console.log(err.sqlMessage);return false;}));
                if(items!=false){
                    res.status(200)
                    .json(items[0]);
                }else{
                    res.status(500)
                    .json({
                        message:"Got an error"
                    });
                }
                con.end();
            }
            break;
        case "PATCH":
            if(!req.body.name || !(req.body.id!=null) ||
                !(req.body.spot_id!=null) || !req.body.description ||
                !(req.body.item_type!=null)|| !(req.body.category!=null) || !req.signedCookies['user_id']
            ){
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else{
                let con = await subd.connect();
                let upd = (await con.query(`udpdate imtes where
                name =?,
                spot_id=?,
                description = ?,
                item_type = ?,
                category = ?
                where
                id = ? and
                user_id = ?
                
                `,[
                    req.body.name,
                    req.body.spot_id,
                    req.body.description,
                    req.body.item,
                    req.body.id,
                    req.signedCookies['user_id'],
                    req.body.category
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    message:"updated rows:"+upd[0].affectedRows
                })
                con.end();
            }
            break;
        case "DELETE":
        if(!req.body.id || !req.signedCookies['user_id']){
            res.status(418)
            .json({
                message:"not enough data"
            })
        }else{
            let con = await subd.connect();
            let del = (await con.query(`DELETE FROM items where id = ? and user_id = ?`,[req.body.id,req.signedCookies['user_id']])
            .catch(err=>{
                res.status(500)
                .json({
                    message:err.sqlMessage
                })
            }));
            con.end();
            res.status(200)
            .json({
                message:"afected rows: "+del[0].affectedRows
            })
            
        }
            break;
       
        default:
            res.status(418)
            .json({
                message:"method error"
            })
    }
})


module.exports = routes;