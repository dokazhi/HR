var routes = require('express').Router(),
fs = require('fs'),
subd = require('../models/subd');

routes.post('/',async (req,res)=>{
    switch(req.body.method){
        case "POST":
            if(!(req.body.type!=null) || !req.files.picture || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else if(!(req.files.picture.mimetype!=='image/png' || req.files.picture.mimetype !== 'image/jpeg')){
                res.status(418)
                .json({
                    message:"wrong data type"
                })
            }else{
                let con = await subd.connect();
                await con.query('START TRANSACTION');
                let create_ba = (await con.query(`
                    INSERT INTO background_avatar SET
                    owner_id = ?,
                    type = ?
                `,[
                    req.signedCookies['user_id'],
                    req.body.type
                ])
                .catch(async err=>{
                    await con.query("ROLLBACK");
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                let ext = req.files.picture.name.substr(req.files.picture.name.length-4)
                let path = `/uploads/ba/ba${create_ba[0].insertId}${ext}`;
                //mae filewrite sync
                fs.writeFileSync("/var/www/projects/team.aida.market/node_hr/public/uploads/ba/ba"+create_ba[0].insertId+ext,req.files.picture.data,"binary");
                    
                        let upd = (await con.query(`update background_avatar set path=? where id =?`,[
                            path,
                            create_ba[0].insertId
                        ])
                        .catch(async err=>{
                            await con.query("ROLLBACK");
                            res.status(500)
                            .json({
                                message:err.sqlMessage
                            })
                        }));
                        await con.query("COMMIT");
                        res.status(200)
                        .json({
                            id:create_ba[0].insertId
                        })
                    

                
                con.end();
            }
            break;
        case "GET":
            let con = await subd.connect();
            let ba = (await con.query(`
            SELECT id,path,type from background_avatar
            `))
            con.end();
            break;
        case "PATCH":
            break;
        case "DELETE":
            break;
        default:
            res.status(418)
            .json({
                message:"Method eror"
            });
            break;
    }
});
module.exports = routes;