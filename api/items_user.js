var routes = require('express').Router(),
subd = require('../models/subd');
routes.post('/',async (req,res)=>{
    console.log(req.body);
    switch(req.body.method){
        case "POST":
            if(!req.body.item_id || !req.body.user_id || !req.signedCookies['user_id'] ){
                res.status(418)
                .json({
                    message:'Not enough data'
                })
            }else{
            let con = await subd.connect();
            await con.query("START TRANSACTION");
            let item = (await con.query(`select user_id from items where id = ?`,[req.body.item_id])
            .catch(err=>{
                res.status(500)
                .json({
                    message:err.sqlMessage
                })
            }));
            if(item[0].length>0){
                console.log(">>>>>>>>>>>>>IN<<<<<<<<<<<<<<<<<<")
                if((item[0][0].user_id == req.signedCookies['user_id'])&&(req.body.user_id != req.signedCookies['user_id'])){
                    let add_item = (await con.query(`
                    INSERT INTO user_items set
                    user_id = ?,
                    item_id = ?
                    `,[
                        req.body.user_id,
                        req.body.item_id
                    ])
                    .catch(async err=>{
                        res.status(500)
                        .json({
                            message:err.sqlMessage
                        })
                    }));
                    await con.query("COMMIT");
                    res.status(201)
                    .json({
                        id:add_item[0].insertId
                    })
                }else{
                    await con.query("ROLLBACK");
                    res.status(500)
                    .json({
                        message:"u cant send this item"
                    })
                }
            }else{
                await con.query("ROLLBACK");
                res.status(500)
                .json({
                    message:"item not found"
                })
            }
            con.end();          
            }
            break;
        case "GET":
            if(!req.body.user_id){
                res.status(418)
                .json({
                    message:"No! Hell NO!"
                })
            }else{
                let con = await subd.connect();
                await con.query(`START TRANSACTION`);
                let item_ids = (await con.query(`
                    SELECT i.id,i.name, i.description, i.spot_id, i.item_type, i.img_path
                    FROM user_items as ui INNER JOIN items as i ON i.id = ui.item_id
                    WHERE ui.user_id = ? and i.deleted=false
                `,[
                    req.body.user_id
                ])
                .catch(async err=>{
                    await con.query("ROLLBACK");
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                if(item_ids[0].length>0){
                    await con.query("COMMIT");
                    res.status(200)
                    .json({
                        body:items_ids[0]
                    })
                }else{
                    await con.query("ROLLBACK");
                    res.status(418)
                    .json({
                        message:"nothing found"
                    })
                }
                con.end();
            }
            break;
        // case "PATCH":
            
        //     break;
        // case "DELETE":
        //     break;
        default:
            res.status(418)
            .json({
                message:"Method eror"
            });
            break;
    }
});
module.exports = routes;