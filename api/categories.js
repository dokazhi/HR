var routes = require('express').Router(),
subd = require('../models/subd');
routes.get("/",async(req,res)=>{
    console.log("tuttt",req.signedCookies['user_id']);
    if(!(req.query.department_id!=null) || !req.signedCookies['user_id']){
        res.status(418)
        .json({
            message:"Sorry no"
        })
    }else{
        let con  = await subd.connect();
        let z = (await con.query(`select id,name from spot where department_id = ?`,[
            req.query.department_id
        ]).catch(err=>{
            return false
        }));
        if(z!=false){
            
                res.status(200)
                .json(
                    z[0]
                )
            
        }else{
            res.status(500)
            .json({
                message:"ERROR!"
            })
        }
        con.end();
    }
})
routes.post("/",async(req,res)=>{
    switch(req.body.method){
        case "GET":
            if(!(req.body.department_id!=null)||!req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"Sorry no"
                })
            }else{
                let con  = await subd.connect();
                let z = (await con.query(`select id,name from spot where department_id = ?`,[
                    req.body.department_id
                ]).catch(err=>{
                    return false
                }));
                if(z!=false){
                    
                        res.status(200)
                        .json({
                            body:z[0]
                        })
                    
                }else{
                    res.status(500)
                    .json({
                        message:"ERROR!"
                    })
                }
                con.end();
            }
            break;
        default:
            res.status(418).
            json({
                message:"Npt ,ejtpd"
            })
    }
});
routes.get("/departments",async(req,res)=>{
    if(req.signedCookies['user_id']){
                
        let con = await subd.connect();
        let get_departments = (await con.query(`SELECT id,name FROM department WHERE user_id = ?`,[req.signedCookies['user_id']])
        .catch(err=>{
            res.status(500)
            .json({
                message:err.sqlMessage
            }).end();
        }));
        res.status(200)
        .json(
            get_departments[0]
        );
        con.end();
    }else{
        res.status(418)
        .json({
            message:"No user_id provided"
        })
    }
})
routes.post("/departments",async (req,res)=>{
    switch (req.body.method){
        case 'POST':
            if(!req.body.name || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:'Not enough data'
                })
            }else{
                let con = await subd.connect();
                let create_dep = (await con.query("INSERT INTO department SET name =?,user_id=?",[
                    req.body.name,
                    req.signedCookies['user_id']
                ])
                .catch(async err=>{
                    console.log('categories:'+err.sqlMessage);
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    }).end();
                }));
                res.status(201)
                .json({
                    id:create_dep[0].insertId
                }).end();
                con.end();
            }
            break;
        case 'GET':
            if(req.signedCookies['user_id']){
                
                    let con = await subd.connect();
                    let get_departments = (await con.query(`SELECT id,name FROM department WHERE user_id = ?`,[req.signedCookies['user_id']])
                    .catch(err=>{
                        res.status(500)
                        .json({
                            message:err.sqlMessage
                        }).end();
                    }));
                    res.status(200)
                    .json({
                        body:get_departments[0]
                    })
                    con.end();
            }else{
                res.status(418)
                .json({
                    message:"No user_id provided"
                })
            }
            break;
        case 'PATCH':
            if(!req.body.department_id || !req.body.name || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"Not enough data"
                })
            }else{
                let con = await subd.connect();
                let update_department  = (await con.query(`UPDATE department SET name=? WHERE id=? AND user_id =? `,[req.body.name,req.body.department_id,req.signedCookies['user_id']])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    message:update_department[0].affectedRows + ' rows updated'
                });
                con.end();
            }
            break;
        case 'DELETE':
            if(!req.body.department_id || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"no department_id provided"
                })
            }else{
                let con = await subd.connect();
                await con.query("START TRANSACTION");
                let delete_dep = (await con.query(`
                    DELETE FROM department WHERE id = ? and user_id =?
                `,[
                    req.body.department_id,
                    req.signedCookies['user_id']
                ]).catch(async err=>{
                    await con.query("ROLLBACK");
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                let delete_spots = (await con.query(`
                    DELETE FROM spot WHERE department_id = ? and user_id =?
                `,[
                    req.body.department_id,
                    req.signedCookies['user_id']
                ])
                .catch(async err=>{
                    await con.query("ROLLBACK");
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }))
                await con.query("COMMIT");
                res.status(200)
                .json({
                    message:'spots deleted:'+delete_spots[0].affectedRows+';departments deleted:'+delete_dep[0].affectedRows
                })
                con.end();
            }
            break;
        default:
            res.status(418)
            .json({
                message:"Invalid method"
            })
    }
   
})
routes.post('/spots',async (req,res)=>{
    switch(req.body.method){
        case "POST":
            if(!req.body.department_id || !req.signedCookies['user_id'] || !req.body.name){
                res.status(418)
                .json({
                    message:'not enough data'
                })
            }else{
                let con = await subd.connect();
                let add_sport = (await con.query(`
                    INSERT INTO spot SET name = ?,department_id = ?,user_id=?
                `,[
                    req.body.name,
                    req.body.department_id,
                    req.signedCookies['user_id']
                ])
                .catch(async err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(201)
                .json({
                    id:add_sport[0].insertId
                });
                con.end();
            }
            break;
        case "GET":
            if(!req.body.department_id || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:'not enough data'
                })
            }else{
                let con = await subd.connect();
                let spots = (await con.query(`
                    SELECT id,name FROM spot WHERE department_id =? AND user_id =?
                `,[
                    req.body.department_id,
                    req.signedCookies['user_id']
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    body:spots[0]
                })
                con.end();
            }
            break;
        case "PATCH":
            if(!req.body.name || !req.body.id || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"Not enough data"
                })
            }else{
                let con = await subd.connect();
                let patch_spot = (await con.query(`
                    UPDATE spot SET name = ? WHERE id =? AND user_id=?
                `,[
                    req.body.name,
                    req.body.id,
                    req.signedCookies['user_id']
                ])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    message:patch_spot[0].affectedRows +': rows affected'
                });
                con.end();
            }
            break;
        case "DELETE":
            if(req.body.id && req.signedCookies['user_id']){
                let con = subd.connect();
                let del_spot = (await con.query(`DELETE * FROM spot WHERE id = ? AND user_id=?`,[req.body.id,req.signedCookies['user_id']])
                .catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                res.status(200)
                .json({
                    message:'spots deleted'+del_spot[0].affectedRows
                })
                con.end();
            }else{
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }
            break;
        default:
            res.status(418)
            .json({
                message:"no method"
            });
            break;
    }
})
module.exports = routes;