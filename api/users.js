/**
 * 
 * METHOD FOR CREATE USERS UNACTIVATED
                let password = generator.generate({
                    length:13,
                    numbers:true,
                });
                let hashed_password = crypto.createHmac('sha1',"hervam").update(password).digest('hex');
                let create_invtied = (await con.query(`
                INSERT INTO user SET
                username = ?,
                invited = true,
                activated = false,
                company_id = ?,
                password = ?,
                spot_id = ?
                `,[
                    req.body.email,
                    req.signedCookies['user_id'],
                    hashed_password,
                    req.body.spot_id
                ]).catch(err=>{
                    
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }))
                if(create_invtied){
                    //todo create invter sender
                    transporter.sendMail({
                        from:"info@team.aida.market",
                        to:req.body.email,
                        subject:"Доступы для авторизации",
                        text:`
                        логин:${req.body.email}
                        пароль:${password}
                        `
                    },(err,info)=>{
                        if(err){
                            console.log(err);
                        }else{
                            console.log(info);
                        }
                    })
                    res.status(201)
                    .json({
                        message:"Invite was sended",
                        id:create_invtied[0].insertId
                    })
                }
 */

'use strict';
var routes = require('express').Router(),
crypto = require('crypto'),
nodemailer = require('nodemailer'),
sendmail = require('sendmail'),
fs = require('fs'),
generator = require('generate-password'),
subd = require('../models/subd');

const SMTPConnection = require('nodemailer/lib/smtp-connection');

let poolConfig = {
    
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use TLS
    auth: {
        user: 'aida.hrm.system@gmail.com',
        pass: '3edc2wsx1qaz'
    },
    tls:{
        rejectUnauthorized:false
    }
};

// let poolConfig = {
    
//     host: 'mail.aida.market',
//     port: 587,
//     debug:true,
    
//     localAddress:false,
//     secure: false,
//     // opportunisticTLS:true, // use TLS
//     ignoreTls:true,
//     authMethod:"PLAIN",
    
//     // connection:true,
//     debug:true,
//     // auth: {
//     //     user: 'info@team.aida.market',
//     //     pass: 'niSSan88'
//     // }
    
// };

// var connection = new SMTPConnection(poolConfig);
var transporter = nodemailer.createTransport(poolConfig);
// transporter.verify((err,suc)=>{
//     if(err){
//         console.log(err);
//     }else{
//         console.log(suc);
//     }
// })
// var connection = new SMTPConnection(options);

routes.post('/',async (req,res)=>{
    console.log(req.body);
    switch(req.body.method){
        case "GETONE":
            if(!req.body.user_id){
                res.status(418)
                .json({
                    message:"what to show"
                }).end();
            }else{
                let con = await subd.connect()
                
                let data = (await con.query(`
                SELECT
                
                ud.data,
                ud.type,
                s.name as spot_name,
                s.id as spot_id,
                u.username,
                u.id as user_id,

                FROM user as u
                INNER JOIN user_data as ud ON u.id = ud.user_id
                INNER JOIN spot as s ON u.spot_id = s.id
                WHERE u.id =? AND u.activated = true
                `,[
                    req.body.user_id
                ]).catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    }).end();
                }));
                if(data[0].length>0){
                    let skills = (await con.query(`
                    SELECT 
                    s.id as skill_id
                    s.name as skill_name,
                    us.percent,
                    s.description,
                    spot.name as spot_name
                    spot.id as spot_id
                    FROM user_skills as us
                    INNER JOIN skills as ON s.id = us.skill_id
                    INNER JOIN spot ON spot.id = s.spot_id
                    WHERE s.deleted = false AND us.activated = true and us.user_id = ?
                    `,[req.body.user_id]));

                    let achievs = (await con.query(`
                    SELECT 
                    s.name as spot_name,
                    a.name as achievement_name,
                    a.id as achievement_id,
                    a.description as description,
                    a.date_achieves as date_achieves,
                    a.point as points,
                    a.path as path,

                    FROM achievements as a
                    INNER JOIN user_achievements as ua ON ua.achievement_id = a.id
                    INNER JOIN spot as s ON a.category_id = s.id
                    where ua.user_id =?
                    `,[
                        req.body.user_id
                    ]).catch(err=>{
                        res.status(500)
                        .json({
                            message:err.sqlMessage
                        }).end();
                    }));
                    let items = (await con.query(`
                    SELECT
                    i.id as item_id,
                    i.name as item_name,
                    i.description as description,
                    i.item_type as item_type,
                    i.img_path as img_path
                    FROM items as i
                    INNER JOIN user_items as ui ON ui.item_id = i.id
                    INNER JOIN spot as s ON i.spot_id = s.id
                    WHERE ui.user_id = ?
                    `,[req.body.user_id]).catch(err=>{
                        res.status(500)
                        .json({
                            message:err.sqlMessage
                        }).end();
                    }));
                    let bas = (await con.query(`
                    SELECT
                    ba.id as ba_id,
                    ba.path as path,
                    ba.type as type
                    FROM background_avatar as ba
                    INNER JOIN user_ba as uba ON uba.id_ba = ba.id
                    WHERE uba.user_id = ?
                    `,[
                        req.body.user_id
                    ]).catch(err=>{
                        res.status(500)
                        .json({
                            message:err.sqlMessage
                        }).end();
                    }));
                    res.status(200)
                    json({
                        body:{
                            data:data[0],
                            items:items[0],
                            bas:bas[0],
                            skills:skills[0],
                            achievements:achiev[0]
                        }
                    }).end();
                }else{
                    res.status(418)
                    .json({
                        message:"USER NOT FOUND"
                    }).end();
                }
                con.end();
            }
            break;
        case "POST":
        
            if(!req.body.email ||!req.body.spot_id || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"NOT ENOUGH DATA"
                })
            }else{
                let con = await subd.connect();
                let exists = (await con.query(`select * from invite_tokens where email=?`,[
                    req.body.email.toLowerCase()
                ]).catch(err=>{
                    return false;
                }));
                if(exists!=false){
                    if(exists[0].length>0){
                        res.status(418)
                        .json({
                            message:"This email allready got invite"
                        })
                    }else{
                        let hash = crypto.createHmac('sha1WithRSAEncryption','hervam').update(req.body.email).digest('hex');
                        let token = (await con.query(`
                            INSERT INTO invite_tokens SET
                            email = ?,
                            token = ?,
                            spot_id = ?,
                            owner_id = ?
                        `,[
                            req.body.email.toLowerCase(),
                            hash,
                            req.body.spot_id,
                            req.signedCookies['user_id']
                        ]).catch(err=>{
                            console.log(err.sqlMessage);
                            return false;
                        }));
                        if(token!=false){
                                let spot = (await con.query(`select name from spot where id = ?`,[req.body.spot_id]).catch(err=>{
                                    console.log(err.sqlMessage);
                                    return false;
                                }));
                                if(spot!=false && spot[0].length>0){
                                    // connection.connect(()=>{
                                    //     console.log("CONNECTED");
                                    // });
                                    // connection.login({
                                    //     credentials:{
                                    //         user: 'info@team.aida.market',
                                    //         pass: 'niSSan88'
                                    //     }
                                    // });
                                    // connection.send({
                                    //     from:"info@team.aida.market",
                                    //     to:req.body.email
                                        
                                    // },`
                                    // Вас пригласили как: ${spot[0][0].name}
                                    // Для регистрации по приглашению пройдите далее:\n
                                    // http://team.aida.market/registration?token=${hash}
                                    // `,(err,info)=>{
                                    //     if(err){
                                    //         console.log(err);
                                    //     }else{
                                    //         console.log(info);
                                    //     }
                                    // })
                                    // connection.close();
                                    transporter.sendMail({
                                        from:"aida.hrm.system@gmail.com",
                                        to:req.body.email,
                                        messageId:hash,
                                        subject:"Приглашение на team.aida.market",
                                        text:`
                                        Вас пригласили как: ${spot[0][0].name}
                                        Для регистрации по приглашению пройдите далее:\n
                                        http://demo.aida.market:9901/#/registrationTwo?token=${hash}
                                        `
                                    },(err,info)=>{
                                        if(err){
                                            console.log(err);
                                        }else{
                                            console.log(info);
                                        }
                                    })
                                    res.status(201)
                                    .json({
                                        message:"Invite was sended",
                                        id:token[0].insertId
                                    })
                                    
                                }
                                //todo create invter sender
                                
                            
                        }
                    }
                }
                
                    
                con.end();
            }

            break;
        case "GET":
            if(!req.signedCookies["user_id"]){
                res.status(418)
                .json({
                    message:"u cant get it!"
                })
            }else{
                let con = await subd.connect();
                let get_users = (await con.query(`
                    select ud.data, u.id, u.username, u.spot_id from user as u
                    INNER JOIN user_data as ud ON ud.user_id = u.id
                    WHERE u.company_id = ?

                `,[
                    req.signedCookies['user_id']
                ]).catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                con.end();
            }
            break;
        case "DELETE":
            if(!req.body.user_id || !req.signedCookies['user_id']){
                res.status(418)
                .json({
                    message:"hell na"
                })
            }else{
                let con = await subd.connect();
                let owner = (await con.query(`
                select company_id from user where id = ?
                `,[
                    req.body.user_id
                ]).catch(err=>{
                    res.status(500)
                    .json({
                        message:err.sqlMessage
                    })
                }));
                if(owner[0].length>0){
                    if(owner[0][0].company_id == req.signedCookies['user_id']){
                        let make_deleted = (await con.query(`
                            UPDATE user set company_id = 0, spot_id = 0
                            where id = ?
                        `,[
                            req.body.user_id
                        ]).catch(err=>{
                            res.status(500)
                            .json({
                                message:err.sqlMessage
                            }).end();
                        }));
                        if(make_deleted[0].affectedRows>0){
                            res.status(200)
                            .json({
                                message:"Faired"
                            }).end();
                        }else{
                            res.status(500)
                            .json({
                                message:"Cant fair it"
                            }).end();
                        }
                    }else{
                        res.status(403)
                        .json({
                            message:"U cant control this user"
                        }).end();
                    }
                }else{
                    res.status(418)
                    .json({
                        message:"wrong id"
                    })
                }
                con.end();
            }
            break;
        case "FILTER":
            if(!req.body.skill_ids || !req.body.ach_ids || !req.body.dep_ids){
                res.status(418)
                .json({
                    message:"not enough data"
                })
            }else{
                let con = await subd.connect();
                let str_skills,str_achs,str_deps;
                if(req.body.skill_ids.length>0){
                    str_skills='(skill.skill_id = '+req.body.skill_ids[0]+" ";
                    for(let i = 1;i<req.body.skill_ids.length;i++){
                        str_skills+="OR skill.skill_id = "+req.body.skill_ids[i]+" "
                    }
                    str_skills+=" )"
                }
                if(req.body.dep_ids.length>0){
                    str_deps = '(dep.id = '+req.body.dep_ids[0]+" ";
                    for(let i = 1;i<req.body.dep_ids.length;i++){
                        str_deps+="OR dep.id = "+req.body.dep_ids[i]+" ";
                    }
                    str_deps+=" )"
                }
                if(req.body.ach_ids.length>0){
                    str_achs = "(ach.id = "+req.body.ach_ids[0]+" ";
                    for(let i =1;i<req.body.ach_ids.length;i++){
                        str_achs+="OR ach.achievement_id = "+req.body.ach_ids[0]+" ";
                    }
                    str_achs+=" )"
                    
                }
                let combo_str =[str_skills,str_deps,str_achs];
                let lat = [];
                for(let i =0;i<combo_str.length;i++){
                    if(combo_str[i]){
                        lat.push(combo_str[i]);
                    }
                }
                // console.log(lat);
                let cmb = lat[0];
                for(let i=1;i<lat.length;i++){
                    cmb+=" AND "+lat[i]
                }
                console.log(cmb);
                
                    let get_users = (await con.query(`
                    SELECT 
                    u.username as username,
                    ud.data as data,
                    u.id as id,
                    ba.path,
                    ba.type
                    FROM user as u
                    LEFT JOIN user_data as ud ON ud.user_id = u.id
                    LEFT JOIN user_achievements as ach ON ach.user_id = u.id
                    LEFT JOIN user_skills as skill ON skill.user_id = u.id
                    LEFT JOIN spot as spot ON u.spot_id = spot.id
                    LEFT JOIN department as dep ON spot.department_id = dep.id
                    LEFT JOIN user_ba as uba ON uba.user_id = u.id
                    LEFT JOIN background_avatar as ba ON uba.id_ba = ba.id
                    WHERE u.activated=true and u.company_id != 0
                    ${(cmb)?"AND "+cmb:""}
                    
                    `,[

                    ]).catch(err=>{
                        console.log(err.sqlMessage);
                        return false;
                    }))
                    console.log(get_users[0]);
                    res.status(200)
                    .json(get_users[0]);
                con.end();
            }
            break;
        case "SEARCH":
            if(!req.body.search){
                res.status(418)
                .json({
                    message:"Have no string"
                })
            }else{
                let con = await subd.connect();
                let like_user = (await con.query(`
                SELECT 
                u.username as username,
                ud.data as data,
                u.id as id,
                ba.path,
                ba.type
                FROM user as u
                LEFT JOIN user_data as ud ON ud.user_id = u.id
                LEFT JOIN user_achievements as ach ON ach.user_id = u.id
                LEFT JOIN user_skills as skill ON skill.user_id = u.id
                LEFT JOIN spot as spot ON u.spot_id = spot.id
                LEFT JOIN department as dep ON spot.department_id = dep.id
                LEFT JOIN user_ba as uba ON uba.user_id = u.id
                LEFT JOIN background_avatar as ba ON uba.id_ba = ba.id
                
                WHERE u.activated=true and ud.data->"$.name" LIKE "%${req.body.search}%";

                `,[]).catch(err=>{console.log(err.sqlMessage);return false;}))
                if(like_user!=false){
                    
                    console.log(like_user[0]);
                    res.status(200)
                    .json(like_user[0])
                }else{
                    res.status(500)
                    .json({
                        message:"SQL ERROR"
                    })
                }
                con.end();
            }
            break;
        default:
            res.status(418)
            .json({
                message:"bad method"
            })
            break;
    }
})

module.exports = routes;