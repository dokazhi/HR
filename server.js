const express = require('express'),
cookieParser = require('cookie-parser'),
bodyParser = require('body-parser'),
busboy = require('busboy-body-parser'),
subd = require('./models/subd'),
app = express(),
ws = require('express-ws')(app),
cors = require('cors'),
apiRouter = express.Router();


//routes for API
var images = require('./api/images'),
categories = require('./api/categories'),//
skills = require('./api/skills'),//
items = require('./api/items'),//
achievements = require('./api/achievements'),//
items_user = require('./api/items_user'),//
ba_user = require('./api/ba_user'),//
background_avatar = require('./api/background_avatar')//
skills_user = require('./api/skills_user'),//
achievs_user = require('./api/achievs_user'),//
pskills_user = require('./api/pskills_user');
pskills = require('./api/personal_skills'),//
auth = require('./api/auth'),//
chat = require('./api/chat'),
users = require('./api/users'),
// quiz = require('./');//
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"); 
    next();
})
global.online = [];    
// process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
//frontEND part
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/'));//js
app.use(express.static(__dirname + '/js'));//js
app.use(express.static(__dirname + '/css'));//js
app.use(express.static(__dirname + '/img'));//js
app.use(express.static(__dirname + '/fonts'));//js
app.use(express.static(__dirname + '/account/js'));//js
app.use(express.static(__dirname + '/account/css'));//js
app.use(express.static(__dirname + '/admin/js'));//js
app.use(express.static(__dirname + '/admin/css'));//js
// app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(busboy());
app.use(cookieParser("ebala"));
app.use('/public',express.static(__dirname+'/public'));


app.listen(9901,()=>{
console.log(9901+'<---Server port demo.aida.market:9901');
});

//API ROUTES
apiRouter.use("/chat",chat);
apiRouter.use("/auth",auth);
apiRouter.use("/images",images);
apiRouter.use("/categories",categories);
apiRouter.use("/skills",skills);
apiRouter.use("/users/skills",skills_user);
apiRouter.use("/personal-skills",pskills);
apiRouter.use("/background-avatar",background_avatar);
apiRouter.use('/users/items',items_user);
apiRouter.use("/users/ba",ba_user);
apiRouter.use("/users/pskills",pskills_user);
apiRouter.use("/users/achievements",achievs_user);
apiRouter.use('/items',items);
apiRouter.use("/achievements",achievements);
apiRouter.use("/users",users);
app.use('/api',apiRouter);




//quiz app
// app.use("/quiz",quiz);//backlog


//Front*1/2BACK Vova
var autorization = require('./routes/autorization'); 
var registration = require('./routes/registration'); 
var registrationTwo = require('./routes/registrationTwo'); 
var account = require('./account/index'); 
var admin = require('./admin/index'); 


app.get("/",(req,res)=>{res.render("index");});
app.get("/logout",(req,res)=>{
res.clearCookie("user_id");
res.redirect("/");
})
app.use("/autorization" , autorization);
app.use("/registration" , registration);
app.use("/registrationTwo" ,registrationTwo);
app.use("/account"  ,account);
app.use("/admin" , admin);
