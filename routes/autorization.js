var express = require('express');
var router = express.Router();
var cookieParser = require('cookie-parser');
var subd = require("../models/subd");

router.use(cookieParser("ebala"));
async function proverkaCookie(req,res,next){
	if(req.signedCookies['user_id']){
		let con = await subd.connect();
		let buffer = await con.query(`SELECT id user_id,company_id FROM user WHERE id = ?`,[req.signedCookies['user_id']]).then(res=>res[0][0]).catch(err=>err.sqlMessage);
		if(buffer){
			if(buffer.company_id==0){res.send({redirect:"/admin"});return false;
			}else{res.send({redirect:"/account"});return false;}
		}
	}
	next();
}
router.use(proverkaCookie)

router.post("/",(req,res)=>{
	res.render("autorization");
})

module.exports = router;
