var express = require('express');
var router = express.Router();
var cookieParser = require('cookie-parser');
var subd = require("../models/subd");

router.use(cookieParser("ebala"));
async function proverkaCookie(req,res,next){
	if(req.signedCookies['user_id']){
		let con = await subd.connect();
		let buffer = await con.query(`SELECT user_id,company_id FROM user WHERE user_id = ?`,[req.signedCookies['user_id']]).then(res=>res[0]).catch(err=>err.sqlMessage);
		con.end();
		if(buffer){
			if(buffer.company_id==0){res.send({redirect:"/admin"});return false;
			}else{res.send({redirect:"/account"});return false;}
		}
	}
	if(!req.body.data.getAttributs.token){res.send({redirect:"/"});return false;}
	let con = await subd.connect();
	let buffer = await con.query(`SELECT email FROM invite_tokens WHERE token = ?`,[req.body.data.getAttributs.token]).then(res=>res[0][0]).catch(err=>err.sqlMessage);
	con.end();
	if(!buffer){res.send({redirect:"/"});return false;}
	next();
}
router.use(proverkaCookie)


router.post("/",(req,res)=>{
	console.log(req.body);
	res.render("registrationTwo");
})

module.exports = router;
