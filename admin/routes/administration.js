var express = require('express');
var router = express.Router();
// var subd = require('../../models/subd');

router.post("/",async (req,res)=>{
	let collection = {};
	console.log(req.signedCookies['user_id'])
	let con = await subd.connect();
	let users = (await con.query(`
	SELECT
		u.id,
		u.username, 
		ud.data,
		spot.name as spot_name,
		spot.id as spot_id,
		ba.path,
		ba.type,
		s.name as skill_name,
		s.id as skill_id,
		d.name depName
	FROM user as u
	LEFT JOIN user_data as ud ON ud.user_id = u.id
	LEFT JOIN user_skills as us ON us.user_id = u.id
	LEFT JOIN skills as s ON us.skill_id = s.id
	LEFT JOIN user_ba as uba ON uba.user_id = u.id
	LEFT JOIN background_avatar as ba ON ba.id = uba.id_ba
	LEFT JOIN spot as spot ON spot.id = u.spot_id
	LEFT JOIN department d ON d.id = spot.department_id
	WHERE u.company_id = ?
	`,[req.signedCookies['user_id']]).catch(err=>{console.log(err.sqlMessage);return false;}))
	if(users!=false){
		collection.users = users[0]
	}else{
		collection.users=[];
	}
	collection['get_departments'] = await con.query(` 	SELECT 
															d.id,
															d.name,
															s.name spname,
															s.id sid
														FROM 
															department d  
														LEFT JOIN 
																spot s 
															ON 
																department_id = d.id 
														WHERE 
															d.user_id = ?`,[req.signedCookies['user_id']]).then(res=>res[0]).catch(err=>err.sqlMessage);
	collection['departments'] = [];
	for (var i = 0; i < collection['get_departments'].length; i++) {
		collection['get_departments'][i]
		let res = 'no';
		for (var j = 0; j < collection['departments'].length; j++) {
			if(collection['departments'][j].id==collection['get_departments'][i].id){res = j;break;}
		}
		if(res=='no'){
			collection.departments.push({
				id:collection['get_departments'][i].id,
				name:collection['get_departments'][i].name,
				spots:[
					{
						id:collection['get_departments'][i].sid,
						name:collection['get_departments'][i].spname
					}
				]
			});
		}else{
			collection.departments[res].spots.push({
				id:collection['get_departments'][i].sid,
				name:collection['get_departments'][i].spname
			});
		}
	}
	con.end();
	res.render("../admin/views/administration",collection);
})

module.exports = router;
