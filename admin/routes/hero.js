var express = require('express');
var router = express.Router();


router.post("/", async (req,res)=>{
	console.log(req.body);
	console.log(req.body.data.getAttributs.id);
	user_id  = req.body.data.getAttributs.id;
	let con = await subd.connect();
	let collection = {};
	let data = (await con.query(`
		SELECT
		ud.data,
		ud.type,
		s.name as spot_name,
		s.id as spot_id,
		u.username,
		u.id as user_id
		FROM user as u
		LEFT JOIN user_data as ud ON u.id = ud.user_id
		LEFT JOIN spot as s on u.spot_id = s.id
		where u.id = ? and u.activated = true
	`,[user_id]).catch(err=>{
		console.log(err);
		return false;
	}));
	if(data!=false){collection.data = data[0];}else{collection.data=[];}
	let skills = (await con.query(`
				SELECT
				s.id as skill_id,
				s.name as skill_name,
				us.percent as skill_percent,
				s.description as skill_description,
				spot.name as spot_name,
				spot.id as spot_id
				FROM user_skills as us
				INNER JOIN skills as s ON s.id=us.skill_id
				INNER JOIN spot as spot ON spot.id = s.spot_id
				WHERE us.activated=true and us.user_id=?
			`,[user_id]).catch(err=>{
				return false;
				console.log(err.sqlMessage);
			}));
	if(skills!=false){collection.skills = skills[0];}else{collection.skills=[];}
	let achievs = (await con.query(`
			SELECT
			s.id as spot_id,
			s.name as spot_name,
			a.name as achiev_name,
			a.id as achiev_id,
			a.description as achiev_description,
			a.date_achieves as achiev_date,
			a.point as achiev_points,
			a.path as achiev_path
			FROM achievements as a
			INNER JOIN user_achievements as ua ON ua.achievement_id = a.id
			INNER JOIN spot as s ON a.category_id = s.id
			WHERE ua.user_id = ?
	`,[user_id]).catch(err=>{console.log(err.sqlMessage);return false;}))
	if(achievs!=false){collection.achievs=achievs[0];}else{collection.achiev=[];}
	let items = (await con.query(`
			SELECT 
			i.id as item_id,
			i.name as item_name,
			i.description as item_description,
			i.item_type as item_type,
			i.img_path as img_path,
			s.id as spot_id
			FROM items as i
			INNER JOIN user_items as ui ON ui.item_id = i.id
			INNER JOIN spot as s ON i.spot_id = s.id
			WHERE ui.user_id = ?
	`,[user_id]).catch(err=>{return false;}));
	if(items!=false){collection.items=items[0];}else{collection.items=[];}
	let backgrounds = (await con.query(`
			SELECT 
			ba.id as background_id,
			ba.path as background_path,
			uba.equiped as equiped
			FROM background_avatar as ba
			INNER JOIN user_ba as uba ON uba.id_ba = ba.id
			WHERE uba.user_id =? and type = false
	`,[user_id]).catch(err=>{return false;}));
	if(backgrounds!=false){collection.backgrounds = backgrounds[0];}else{collection.backgrounds=[];}
	let avatars = (await con.query(`
			SELECT
			ba.id as avatar_id,
			ba.path as avatar_path,
			uba.equiped as equiped
			FROM background_avatar as ba
			INNER JOIN user_ba as uba on uba.id_ba = ba.id
			WHERE uba.user_id=? and type = true
	`,[user_id]).catch(err=>{return false;}));
	if(avatars!=false){collection.avatars = avatars[0];}else{collection.avatars=[];}
	let personal_skills = (await con.query(`
			select
			ps.id as pskill_id,
			ps.name as pskill_name,
			ps.description as pskill_description
			FROM personal_skills as ps
			INNER JOIN pskills_user as psu ON psu.pskill_id = ps.id
			WHERE psu.user_id=?
	`,[user_id]).catch(err=>{return false;}));
	if(personal_skills!=false){collection.pskills=personal_skills[0];}else{collection.pskills=[];}

   console.log(collection);
    
    
    con.end();
	res.render("../admin/views/hero",collection);
})

module.exports = router;
