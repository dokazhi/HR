var express = require('express');
var router = express.Router();
var subd = require('../../models/subd');

router.post("/",async (req,res)=>{
	let con = await subd.connect(),result = {};
	console.log(req.signedCookies['user_id']);
	result['get_departments'] = await con.query(` 	SELECT 
														d.id,
														d.name 
													FROM department d
													WHERE 
														d.user_id = ?`,[req.signedCookies['user_id']]).then(res=>res[0]).catch(err=>err.sqlMessage);	
	result['get_users'] = await con.query(`	SELECT 
												ud.data, 
												u.id, 
												u.username, 
												u.spot_id
											FROM user as u
												LEFT JOIN user_data as ud 
													ON 
														ud.user_id = u.id
											WHERE 
												u.company_id = ?

										`,[
											req.signedCookies['user_id']
										]).then(res=>res[0]).catch(err=>err.sqlMessage);	
	console.log(result['get_users']);
	res.render("../admin/views/dispatch",result);
});


let nodemailer = require('nodemailer');
let poolConfig = {
    
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use TLS
    auth: {
        user: 'aida.hrm.system@gmail.com',
        pass: '3edc2wsx1qaz'
    },
    tls:{
        rejectUnauthorized:false
    }
};
var transporter = nodemailer.createTransport(poolConfig);

router.post('/send',async (req,res)=>{
	let body = JSON.parse(req.body.body);
	let file = req.files;
	let con = await subd.connect();
	let depUsers = [];
	function sendMail(to,file,message){
		if(Object.keys(file).length){
			let attach = [{
				filename: file.file.name,
				content: file.file.data
			}];
			transporter.sendMail({
				                    from:"aida.hrm.system@gmail.com",
				                    to:to,
				                    subject:"Рассылка с team.aida.market",
				                    text:(message)?message:"",
				                    attachments:attach
				                },(err,info)=>{
				                    if(err){
				                        console.log(err);
				                    }else{
				                        console.log(info);
				                    }
				                });	
		}else{
			transporter.sendMail({
				                    from:"aida.hrm.system@gmail.com",
				                    to:to,
				                    subject:"Рассылка с team.aida.market",
				                    text:message
				                },(err,info)=>{
				                    if(err){
				                        console.log(err);
				                    }else{
				                        console.log(info);
				                    }
				                });
		}
	}
	for (var i = 0; i < body.departArray.length; i++) {
		depUsers.push(await con.query(`	SELECT u.username
									FROM user u
									INNER JOIN spot s 
										ON s.id = u.spot_id and	s.department_id = ?
										`,[body.departArray[i]]).then(res=>res[0]).catch(err=>err.sqlMessage));
	}
	
	for (var i = 0; i < depUsers.length; i++) {
		for (var j = 0; j < depUsers[i].length; j++) {
			console.log(depUsers[i][j].username);
			sendMail(depUsers[i][j].username,file,body.message);
		}
	}
	console.log(body);
	// console.log(body.cooperatorArr)
	for (var i = 0; i < body.cooperatorArray.length; i++) {
		let email = await con.query(`	SELECT u.username
										FROM user u
										WHERE u.id = ?
											`,[body.cooperatorArray[i]]).then(res=>res[0][0]).catch(err=>err.sqlMessage);
		sendMail(email.username,file,body.message);
	}
	// console.log(depUsers);
	con.end();

	
	res.send(body);
});

module.exports = router;
