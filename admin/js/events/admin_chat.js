var socket = new WebSocket('ws://demo.aida.market:9901/api/chat');
// socket.onclose((event)=>{
//     console.log(event);
//     console.log("socket is closed")
// });
socket.onerror = function(error) {
    console.log(error);
};
socket.onclose = function(event) {
    if (event.wasClean) {
      console.log('Соединение закрыто чисто');
    } else {
        console.log('Обрыв соединения'); // например, "убит" процесс сервера
    }
    console.log('Код: ' + event.code + ' причина: ' + event.reason);
  };
socket.onmessage = function(event) {
    console.log(event);
    console.log(event.data);
    let inc_data = JSON.parse(event.data);
        let new_message = document.createElement("div");
        new_message.className = "mess fl-r ta-l";
        new_message.innerHTML=`
        <div class="left-mess">
								<p>${inc_data.message}</p>
							</div>
        `
        let message_area = document.getElementsByClassName("mess-area")[0].appendChild(new_message);    
    

};
socket.onopen = function() {
    console.log(socket.socket_id);
};

var send_btn = document.getElementsByClassName("send_mess_btn")[0].onclick = async (e)=>{
    let text_data = document.getElementsByClassName("text_message")[0].value;
    let id = document.querySelector(".chat-name").getAttribute('id');
    if(text_data!=""){
        let data = {
            user_id:id,
			message:text_data
		}
		let filtered = await fetch("/api/chat",{
			credentials: "include",
			method:"POST",
			headers:{
				"content-type":"application/json"
			},
			body:JSON.stringify(data)})
			.then(res=>res.json());
			console.log(filtered);
        let new_message = document.createElement("div");
        new_message.className = "mess fl-r ta-r";
        new_message.innerHTML=`
        <div class="right-mess">
                                <p>${text_data}</p>
                            </div>
        `;
        document.getElementsByClassName("subscripeMessArea")[0].appendChild(new_message);   
        console.log(document.querySelector('.mess-area'));
    }
    
}

var contact_list = document.getElementsByClassName("contacts-list")[0].children;
for(let i =0;i<contact_list.length;i++){
    contact_list[i].onclick = async (e)=>{
        let chat_title = document.getElementsByClassName("chat-title")[0].children[1];
        document.querySelector('textarea').removeAttribute("disabled");
        console.log("tut");
        document.querySelector('.subscripeMessArea').innerHTML = ``;
        let buffer = await fetch('/api/chat/messages',{method:"POST",body:JSON.stringify({user_id:contact_list[i].getAttribute("id")}),credentials:'include',headers:{'content-type':'application/json'}}).then(res=>res.json()).catch(err=>err);
        console.log(buffer);
        for (var j = 0; j < buffer.length; j++) {
            buffer[i]
            let div = document.createElement('div');
            console.log(buffer[j].receiver_id,contact_list[i].getAttribute("id"));
            if(buffer[j].receiver_id==contact_list[i].getAttribute("id")){
                div.className = 'mess fl-r ta-r';    
                div.innerHTML = `<div class="right-mess">
                                    <p>${buffer[j].message}</p>
                                </div>`;
                document.querySelector('.subscripeMessArea').appendChild(div);
            }else{
                div.className = 'mess fl-r ta-l';    
                div.innerHTML = `<div class="left-mess">
                                    <p>${buffer[j].message}</p>
                                </div>`;
                document.querySelector('.subscripeMessArea').appendChild(div);
            }
            
        }
        chat_title.innerHTML = contact_list[i].children[1].innerHTML;
        chat_title.setAttribute("id",contact_list[i].getAttribute("id"));
        if(contact_list[i].children[0].classList.contains("status-on")){
            document.getElementsByClassName("chat-status-on")[0].innerHTML="Онлайн";
        }else if(contact_list[i].children[0].classList.contains("status-off")){
            document.getElementsByClassName("chat-status-on")[0].innerHTML="Оффлайн";
        }

    }
}

