let functionNavigation = {
	clickPage: async function(self) {
		let state = { 'page_id': self.getAttribute('data-pageId'), 'page': self.getAttribute("data-page")},title='',id='';
		(self.getAttribute("data-id"))?id = `?id=${self.getAttribute("data-id")}`:"";
		let url = `http://${location.hostname}:9901/admin/#/${self.getAttribute("data-page")}${id}`;
		let data = {
			page: self.getAttribute('data-page'),
			getAttributs:dataObject.getUrlGet(url)
		};
		console.log(data);
		window.history.pushState(state, title, url);
		let html = await this.ajaxPage(data);
		this.loadingAsyncScripts(data.page,html);
	},
	inLoadingPage: async function(data) {
		let html = await this.ajaxPage(data);
		this.loadingAsyncScripts(data.page,html);
	},
	loadingAsyncScripts: function(page,html){
		let headerItem = document.querySelector(`.header-item[data-page="${page}"]`),
			activeHeaderItem = document.createElement("span"),
			eventScript = document.createElement("script"),
			main = document.getElementsByClassName("main")[0],
			functionScript = document.createElement("script");
		functionScript.src = `/functionsPage/admin_${page}.js`;
		eventScript.src = `/events/admin_${page}.js`;
		if(document.querySelector(".active"))document.querySelector(".active").querySelector('.headerItemActive').remove();		
		activeHeaderItem.className = 'headerItemActive';
		activeHeaderItem.innerHTML = `	<hr>
										<div class="circle"></div>`;
		main.innerHTML = html;
		main.appendChild(functionScript);
		main.appendChild(eventScript);
		if(document.querySelector(".active"))document.querySelector(".active").classList.remove("active");
		if(headerItem){
			headerItem.classList.add("active");
			headerItem.appendChild(activeHeaderItem);			
		}

	},
	ajaxPage:async data => {
		let buffer = await fetch(`/admin/${data.page}`,{
									method:"POST",
									body:JSON.stringify({data}),
									headers:{
										'Content-Type':'application/json'
									},
									credentials: 'include',
								}).then(res=>res.text());
		try{
			buffer = JSON.parse(buffer);
			console.log(buffer);
			if(buffer.redirect)window.location.replace(`${buffer.redirect}`);
			return false;
		}catch (err){
			return buffer;	
		}
		return buffer;
	}
};