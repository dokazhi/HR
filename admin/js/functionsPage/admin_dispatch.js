functions = {
	async dispatch(){
		let cooperator = document.querySelector(".cooperat"),depart = document.querySelector(".depart");
		let data = {};
		let fs = new FormData();
		data['cooperatorArray'] = [];
		data['departArray'] = [];
		for (var i = 0; i < cooperator.querySelectorAll("p").length; i++) {
			let input = cooperator.querySelectorAll("p")[i].querySelector('input');
			if(input.checked)data['cooperatorArray'].push(+input.getAttribute("data-id"));
		}
		for (var i = 0; i < depart.querySelectorAll("p").length; i++) {
			let input = depart.querySelectorAll("p")[i].querySelector('input');
			if(input.checked)data['departArray'].push(+input.getAttribute('data-id'));
		}
		if(document.querySelector("input[type='file']").files[0])fs.append('file',document.querySelector("input[type='file']").files[0]);
		if(document.querySelector("textarea").value)data['message'] = document.querySelector("textarea").value;
		if(!data['cooperatorArray'].length&&!data['departArray'].length)return false;
		if(!data['message']&&!document.querySelector("input[type='file']").files[0])return false;
		fs.append("body",JSON.stringify(data));
		let test = await fetch('/admin/dispatch/send',{method:"POST",body:fs,credentials:"include"}).then(res.json()).catch(err=>err);
		console.log(data);
	}
}