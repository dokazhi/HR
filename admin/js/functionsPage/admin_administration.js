admin = {
	clickObject(self){
		self = (self.className.search(/object/)!=1)?self:self.closest(".object");
		(self.className.search(/active/)==-1)?self.classList.add('active'):self.classList.remove('active');
	},
	async popup(){
		let buffer = {};
		buffer['departments'] = await fetch("/api/categories/departments",{credentials:'include'}).then(res=>res.json());
		buffer['position'] = await fetch("/api/categories?department_id=1",{credentials:'include'}).then(res=>res.json());
		let selectDepartment = document.createElement("select"),optionDepartment,selectPosition = document.createElement("select"),optionPosition;
		selectDepartment.classList.add("getPositionForDepart")
		for (var i = 0; i < buffer['departments'].length; i++) {
			optionDepartment = document.createElement("option");
			optionDepartment.value = buffer['departments'][i]['id'];
			optionDepartment.innerText = buffer['departments'][i]['name'];
			selectDepartment.appendChild(optionDepartment);
		}
		for (var i = 0; i < buffer['position'].length; i++) {
			optionPosition = document.createElement("option");
			optionPosition.value = buffer['position'][i]['id'];
			optionPosition.innerText = buffer['position'][i]['name'];
			selectPosition.appendChild(optionPosition);	
		}
		document.querySelector('.popup').style.width = "400px";
		document.querySelector('.popup').style.height = "300px";
		document.querySelector('.popup').innerHTML = `
			<div class="content">
				<input type="email" placeholder="Почтовый адрес" />
			</div>
		`;
		document.querySelector(".popup").children[0].appendChild(selectDepartment);
		document.querySelector(".popup").children[0].appendChild(selectPosition);
		let div = document.createElement("div");
		div.classList.add("button");
		div.innerHTML = `<div class="deleteButton insert">Отправить</div>`;
		document.querySelector(".popup").children[0].appendChild(div);
		document.querySelector(".popup").style.display = 'block';
		document.querySelector(".subscripePopUp").style.display = 'block';
	},
	async getPositionForDepart(self){
		console.log(self.value);
		let positions = await fetch(`/api/categories?department_id=${self.value}`,{credentials:'include'}).then(res=>res.json());
		let select = self.parentElement.querySelectorAll("select")[1],optionPosition;
		select.innerHTML = '';
		for (var i = 0; i < positions.length; i++) {
			optionPosition = document.createElement("option");
			optionPosition.value = positions[i]['id'];
			optionPosition.innerText = positions[i]['name'];
			select.appendChild(optionPosition);	
		}
		console.log(positions);
	},
	successedPers(self){
		document.querySelector(".blockBaseInformation").removeAttribute("style");
		for (var i = 0; i < document.querySelectorAll(".object").length; i++){document.querySelectorAll(".object")[i].classList.remove("active");}
	},
	async invite(){
		data = {
			method:"POST",
			email:document.querySelector(".popup input").value,
			spot_id:document.querySelectorAll(".popup select")[1].value
		};
		let buffer = await fetch("/api/users",{	method:"POST",
												headers:{	'Content-Type': 'application/json',
														 	'Access-Control-Allow-Origin':'true'},
												body:JSON.stringify(data),
												credentials:"include"}).then(res=>res.json());
		console.log(buffer);
		document.querySelectorAll(".popup")[0].style.display = 'none';
		alert(`${document.querySelector(".popup input").value} успешно приглашен!!`);
	},
	headerClick(self){
		let thisIndex = [].indexOf.call(document.querySelectorAll(".header h2"),self);
		let activeIndex = [].indexOf.call(document.querySelectorAll(".header h2"),document.querySelector(".header h2.active"));
		if(thisIndex==activeIndex)return false;
		document.querySelector(".header h2.active").classList.remove("active");
		self.classList.add("active");
		if(document.querySelector(".add").className.search(/addPers/)!=-1){
			document.querySelector(".add").classList.remove("addPers");
			document.querySelector(".add").classList.add("addDivision");
		}else{
			document.querySelector(".add").classList.remove("addDivision");
			document.querySelector(".add").classList.add("addPers");
		}
		document.querySelectorAll(".tab")[thisIndex].removeAttribute('style');
		document.querySelectorAll(".tab")[activeIndex].setAttribute("style","display:none");
		document.querySelectorAll(".blockBaseInformation")[activeIndex].setAttribute("style","display:none");
	},
	editDivision(self){
		let parent = self.closest(".content"),input;
		let div = document.createElement("div");
		div.classList.add("add");
		div.classList.add("addPodDiv");
		parent.children[0].appendChild(div);
		for (var i = 0; i < parent.children[0].querySelectorAll("p").length; i++) {
			parent.children[0].querySelectorAll("p")[i].setAttribute("style","display:none");
			input = document.createElement("div");
			input.classList.add("positionDivInput");
			input.innerHTML = `<input type="text"><div class="delInput"></div>`;
			input.querySelector("input").value = parent.children[0].querySelectorAll("p")[i].innerText;
			parent.children[0].appendChild(input);
		}	
		self.closest(".divisionsBlock").querySelector("span").setAttribute("style","display:none");
		input = document.createElement("input");
		input.setAttribute('type','text');
		input.value = self.closest(".divisionsBlock").querySelector("span").innerText;
		self.closest(".divisionsBlock").children[0].appendChild(input);
		parent.children[1].children[0].classList.remove("redit");
		parent.children[1].children[0].classList.add("save");
		parent.children[1].children[0].innerText = "Сохранить";
		parent.children[1].children[1].classList.remove("del");
		parent.children[1].children[1].classList.add("cancel");
		parent.children[1].children[1].innerText = "Отменить";
	},
	cancelDivision(self){
		let parent = self.closest(".content"),p,buffer = [];
		while(parent.children[0].querySelector(".positionDivInput")){parent.children[0].querySelector(".positionDivInput").remove();}
		for (var i = 0; i < parent.children[0].querySelectorAll("p").length; i++){parent.children[0].querySelectorAll("p")[i].removeAttribute("style");}
		self.closest(".divisionsBlock").querySelector("input").remove();
		self.closest(".divisionsBlock").querySelector("span").removeAttribute("style");
		parent.children[1].children[0].classList.remove("success");
		parent.children[1].children[0].classList.add("redit");
		parent.children[1].children[0].innerText = "Редактировать";
		parent.children[1].children[1].classList.remove("cancel");
		parent.children[1].children[1].classList.add("del");
		parent.children[1].children[1].innerText = "Удалить";
		parent.querySelector(".addPodDiv").remove();
	},
	async deleteDivision(self){
		let parent = self.closest(".divisionsBlock");
		let data = {
			method:'DELETE',
			department_id:parent.getAttribute('data-id')
		};
		let buffer = await fetch("/api/categories/departments",{method:"POST",body:JSON.stringify(data),headers:{'Content-Type':'application/json'},credentials:'include'}).then(res=>res.json()).catch(err=>err);
		console.log(buffer);
		self.closest(".divisionsBlock").remove();
	},
	saveDivision(self){
		let parent = self.closest(".content"),p,buffer={position:[]};
		for (var i = 0; i < document.querySelectorAll("input").length; i++) {
			if(parent.querySelectorAll("p")[i]&&parent.querySelectorAll("input")[i]){
				parent.querySelectorAll("p")[i].innerText = parent.querySelectorAll("input")[i].value;
				buffer['position'].push(parent.querySelectorAll("input")[i].value);
				parent.children[0].querySelectorAll("p")[i].removeAttribute("style");	
			}else if(parent.querySelectorAll("p")[i]&&!parent.querySelectorAll("input")[i]){
				parent.querySelectorAll("p")[i].remove();
			}else if(parent.querySelectorAll("input")[i]){
				p = document.createElement("p");
				p.innerText = parent.querySelectorAll("input")[i].value;
				parent.children[0].appendChild(p);
			}
		}
		parent.querySelector(".addPodDiv").remove();
		while(parent.children[0].querySelector(".positionDivInput")){parent.children[0].querySelector(".positionDivInput").remove();}
		buffer['division'] = self.closest(".divisionsBlock").querySelector("input").value;
		self.closest(".divisionsBlock").querySelector("span").innerText = self.closest(".divisionsBlock").querySelector("input").value;
		self.closest(".divisionsBlock").querySelector("input").remove();
		self.closest(".divisionsBlock").querySelector("span").removeAttribute("style");
		parent.children[1].children[0].classList.remove("success");
		parent.children[1].children[0].classList.add("redit");
		parent.children[1].children[0].innerText = "Редактировать";
		parent.children[1].children[1].classList.remove("cancel");
		parent.children[1].children[1].classList.add("del");
		parent.children[1].children[1].innerText = "Удалить";
		console.log(buffer);
	},
	addPodDiv(self){
		let parent = self.closest(".content");
		let input = document.createElement("div");
		input.classList.add("positionDivInput");
		input.innerHTML = `<input type="text"><div class="delInput"></div>`;
		parent.children[0].appendChild(input);
	},
	addDivision(){document.querySelectorAll(".blockBaseInformation")[1].removeAttribute('style');},
	addPosition(){
		let div = document.createElement("div");
		div.classList.add("inputPack");
		div.innerHTML = `	<input type="text" placeholder="Должность">
							<input type="text" placeholder="Должность">`;
		document.querySelector(".subscripePosition").appendChild(div);
	},
	async insertDivision(){
		let data = {},elements = document.querySelectorAll(".blockBaseInformation")[1].querySelector(".subscripePosition").querySelectorAll("input");
		data['departments'] = {};data['position'] = {};
		data['departments']['name'] = document.querySelectorAll(".blockBaseInformation")[1].querySelector("input").value;data['position']['name'] = [];
		for (var i = 0; i < elements.length; i++) {(elements[i].value!="")?data['position']['name'].push(elements[i].value):"";}
		if(data['departments']['name']=="")return false;
		if(!data.position['name'])return false;
		data['departments'].method = "POST";
		data['position'].method = 'POST';
		data['position'].department_id = await fetch('/api/categories/departments',{method:"POST",body:JSON.stringify(data['departments']),headers:{'Content-Type':'application/json'},credentials:'include'}).then(res=>res.json()).catch(err=>err);
		for (var i = 0; i < data['position']['name'].length; i++) {
			let buffer = {
				department_id:data['position']['department_id'].id,
				method:"POST",
				name:data['position']['name'][i]
			};
			await fetch('/api/categories/spots',{method:"POST",body:JSON.stringify(buffer),headers:{'Content-Type':"application/json"},credentials:"include"}).then(res=>res.json()).catch(err=>err);
		}
		document.querySelectorAll(".blockBaseInformation")[1].style.display = 'none';
		alert("Отдел успешно добавлен");
		let div = document.createElement('div');
		div.classList.add('divisionsBlock');
		div.setAttribute('data-id',data['position'].department_id.id);
		let childDiv = document.createElement('div');
		childDiv.classList.add('pPack');
		for (var i = 0; i < data['position']['name'].length; i++) {
			let p = document.createElement('p');
			p.innerText = data['position']['name'][i];
			childDiv.appendChild(p);
		}
		div.innerHTML = `<div class="header">
							<span>${data['departments']['name']}</span>
						</div>
						<div class="content">
							
							<div class="butPack">
								<div class="editButton del">Удалить</div>
							</div>
						</div>`;
		div.querySelector('.content').insertBefore(childDiv,div.querySelector('.butPack'));
		document.querySelector('.divisions').appendChild(div);
	},
	deleteInput(self){self.parentElement.remove();},
	revealInfo(self){
		if(self.className.search(/noactive/)!=-1){
			self.style.transform = 'rotate(180deg)';
			self.classList.remove("noactive");
			self.classList.add("active");
			self = self.closest(".infoPers");
			self.querySelector(".content").style.height = '382px';	
		}else{
			self.style.transform = 'rotate(0deg)';
			self.classList.add("noactive");
			self.classList.remove("active");
			self = self.closest(".infoPers");
			self.querySelector(".content").style.height = '170px';	
		}
		console.log(self);
	},
	addObject(self){
		self = self.closest(".insertSkill");
		let div = document.createElement('div');
		div.classList.add("object");
		div.innerHTML = `<div class="choiseCircle"></div>
						 <p>${self.querySelector('input').value}</p>`;
		document.querySelector(".subscripeObjects").appendChild(div);
		console.log("tut",self);
	}
}