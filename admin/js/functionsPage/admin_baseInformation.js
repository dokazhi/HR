baseInformation = {
	achievementsTemplate(){
		return `<div class="content">
					<div class="inputPack">
						<input type="text" name="name" placeholder="Введите наименование">
						<input type="text" name="point" placeholder="Введите очки">
					</div>
					<div class="block-add-file">
						<input type="file">
						<p>Прикрепить файл<img src="img/document.png" alt=""></p>
					</div>
					<textarea name="description" placeholder="Описание"></textarea>
					<div class="button">
						<div class="deleteButton insert">ок</div>
					</div>
					
				</div>`;
	},
	skillsTemplate(){
		return `<div class="content">
					<p><input type="text" name="name" placeholder="Введите наименование"></p>
					<textarea placeholder="Описание"></textarea>
					<div class="button">
						<div class="deleteButton insert">ок</div>
					</div>
					
				</div>`;
	},
	itemsTemplate(){
		return `<div class="content">
					<p><input type="text" name="name" placeholder="Введите наименование"></p>
					<select class="type_item">
						<option value='1'>Голова</option>
						<option value='2'>Броня</option>
						<option value='3'>Штаны</option>
						<option value='4'>Ботинки</option>
						<option value='5'>Перчатки</option>
					</select>
					<div class="block-add-file">
						<input type="file">
						<p>Прикрепить файл<img src="img/document.png" alt=""></p>
					</div>
					<textarea name="description" placeholder="Описание" style="height:200px;"></textarea>
					<div class="button">
						<div class="deleteButton insert">ок</div>
					</div>
					
				</div>`;

	},
	popup(){
		document.querySelector('.popup').style.width = "700px";
		document.querySelector('.popup').style.height = "500px";
		let departaments = ['achievements','skills','items'];
		let departamentsId = +document.querySelectorAll(".blockBaseInformation")[0].querySelector('p.active').getAttribute('id')-1;
		document.querySelector('.popup').innerHTML = baseInformation[`${departaments[departamentsId]}Template`]();
		document.querySelector(".popup").style.display = 'block';
		document.querySelector(".subscripePopUp").style.display = 'block';
	},
	async clickCheck(self){
		self.closest(".blockBaseInformation").querySelector('p.active').classList.remove("active");
		self.classList.add("active");
		let couple = document.querySelectorAll(".blockBaseInformation p.active");
		// console.log(couple[0].getAttribute('id')+"CAEGORY");
		// console.log(couple[1].getAttribute('id')+"THIS IS SPOT")
		let data,head,list;
		switch(couple[0].getAttribute('id')){
			case "1":
			
				data = {
					method:"GETBYSPOT",
					spot_id:couple[1].getAttribute('id')
				}
				let achieves = await fetch("/api/achievements",{
					credentials: "include",
					method:"POST",
					headers:{
						"content-type":"application/json"
					},
					body:JSON.stringify(data)})
				.then(res=>res.json());
				console.log(achieves);
				head = document.getElementsByClassName("centerBlockBaseInformation")[0];
				
				head.querySelector("h1").innerText = 'Достижения';
				list = document.getElementsByClassName("list_content")[0];
				list.innerHTML="";
				for(let i =0;i<achieves.length;i++){
					let ach = document.createElement("div");
					ach.innerHTML=`
						<div class="header">
							<div class="del" data-id="${achieves[i].id}"></div>
							<h3>${achieves[i].name}</h3>
						</div>
						<div class="info_image">
							<img src="/public${achieves[i].path}" />
						</div>
						<div class="content">
							<p>${achieves[i].description}</p>
						</div>
					`
					ach.classList.add("info");
					list.appendChild(ach);
					
				}
				// self.closest(".blockBaseInformation").querySelector('p.active')
			break;
			case "2":
			data = {
				method:"GET_SPOT",
				spot_id:couple[1].getAttribute('id')
			}
			let skills = await fetch("/api/skills",{
				credentials: "include",
				method:"POST",
				headers:{
					"content-type":"application/json"
				},
				body:JSON.stringify(data)})
			.then(res=>res.json());
			console.log(skills);
			head = document.getElementsByClassName("centerBlockBaseInformation")[0];
			
			head.querySelector("h1").innerText = 'Навыки';
			list = document.getElementsByClassName("list_content")[0];
			list.innerHTML="";
			for(let i =0;i<skills.length;i++){
				let ach = document.createElement("div");
				ach.innerHTML=`
					<div class="header">
						<div class="del" data-id="${skills[i].id}"></div>
						<h3>${skills[i].name}</h3>
					</div>
					<div class="content">
						<p>${skills[i].description}</p>
					</div>
				`
				ach.classList.add("info");
				list.appendChild(ach);
				
			}
			break;
			case "3":
			data = {
				method:"GETBYSPOT",
				spot_id:couple[1].getAttribute('id')
			}
			let items = await fetch("/api/items",{
				credentials: "include",
				method:"POST",
				headers:{
					"content-type":"application/json"
				},
				body:JSON.stringify(data)})
			.then(res=>res.json());
			
			head = document.getElementsByClassName("centerBlockBaseInformation")[0];
			
			head.querySelector("h1").innerText = 'Предметы';
			list = document.getElementsByClassName("list_content")[0];
			list.innerHTML="";
			for(let i =0;i<items.length;i++){
				let ach = document.createElement("div");
				ach.innerHTML=`
					<div class="header">
						<div class="del" data-id="${items[i].id}"></div>
						<h3>${items[i].name}</h3>
					</div>
					<div class="info_image">
						<img src="/public${items[i].img_path}" />
					</div>
					<div class="content">
						<p>${items[i].description}</p>
					</div>
				`
				ach.classList.add("info");
				list.appendChild(ach);
				
			}
			break;
			
		}
		// for (var i = 0; i < document.querySelectorAll(".blockBaseInformation p.active").length; i++) {
		// 	console.log(document.querySelectorAll(".blockBaseInformation p.active")[i].getAttribute('id'));
		// }
	},
	delete(self){
		let departaments = ['achievements','skills','items'];
		let departamentsId = +document.querySelectorAll(".blockBaseInformation")[0].querySelector('p.active').getAttribute('id')-1;
		this[`${departaments[departamentsId]}Delete`](self);
	},
	add(){
		let name = document.querySelector(".popup input").value;
		let description = document.querySelector(".popup textarea").value;
		let departaments = ['achievements','skills','items'];
		let departamentsId = +document.querySelectorAll(".blockBaseInformation")[0].querySelector('p.active').getAttribute('id')-1;
		this[`${departaments[departamentsId]}Save`]();
	},
	async getBase64(file) {
	    return new Promise(function(resolve, reject) {
	        var reader = new FileReader();
	        reader.onload = function() { resolve(reader.result); };
	        reader.onerror = reject;
	        reader.readAsDataURL(file);
	    });
	},
	async achievementsSave(){
		let files = document.querySelector(".popup input[type='file']").files;
		let img = await this.getBase64(files[0]).then(res=>res);
		let name = document.querySelector(".popup input[name='name']").value;
		let point = document.querySelector(".popup input[name='point']").value;
		let description = document.querySelector(".popup textarea").value;
		let positionId = document.querySelectorAll(".blockBaseInformation")[1].querySelector('p.active').getAttribute('id');
		let data = new FormData();
		if(name=="")return false; else data.append('name',name);
		if(point=="")return false; else data.append('points',point);
		if(description=="")return false; else data.append('description',description);
		if(!files[0])return false; else data.append('picture',files[0]);
        data.append('category_id',positionId);
        let date = new Date();
        console.log(`${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`);
        data.append('date_achieves',`${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`);
        data.append('method',"POST");
        let buffer = await fetch("/api/achievements",{method:"POST",body:data,credentials:'include'}).then(res=>res.json()).catch(err=>err);
  		let div = document.createElement('div');
  		div.classList.add('info');
  		div.innerHTML = `	<div class="header">
								<div class="del" data-id="${buffer.id}"></div>
								<h3>${name}</h3>
							</div>
							<div class="info_image">
									<img src="${img}" />
							</div>
							<div class="content">
								<p>${description}</p>
							</div>`;
		document.querySelector('.list_content').appendChild(div);
		document.querySelector(".popup").style.display = 'none';
		document.querySelector(".subscripePopUp").style.display = 'none';
	},
	async skillsSave(){
		let name = document.querySelector('.popup input[name="name"]').value;
		let description = document.querySelector('.popup textarea').value;
		let spot_id = document.querySelectorAll(".blockBaseInformation")[1].querySelector('p.active').getAttribute('id');
		let data = new FormData();
		console.log(name,description);
		if(name=="")return false; else data.append('name',name);
		if(description=="")return false; else data.append('description',description);
		data.append('spot_id',spot_id);
		data.append('status',1);
		data.append('method','POST');
		let buffer = await fetch('/api/skills',{method:'POST',body:data,credentials:'include'}).then(res=>res.json()).catch(err=>err);
		console.log(buffer);
		let div = document.createElement('div');
  		div.classList.add('info');
  		div.innerHTML = `	<div class="header">
								<div class="del" data-id="${buffer.id}"></div>
								<h3>${name}</h3>
							</div>
							<div class="content">
								<p>${description}</p>
							</div>`;
		document.querySelector('.list_content').appendChild(div);
		document.querySelector(".popup").style.display = 'none';
		document.querySelector(".subscripePopUp").style.display = 'none';	
	},
	async itemsSave(){
		console.log(document.querySelectorAll('.blockBaseInformation')[1]);
		let name = document.querySelector(".popup input[name='name']").value,
			description = document.querySelector('.popup textarea').value,
			spot_id = document.querySelectorAll('.blockBaseInformation')[1].querySelector('p.active').getAttribute('id'),
			category = document.querySelector('.popup .type_item').value,
			file = document.querySelector('.popup input[type="file"]').files;
		let img = await this.getBase64(file[0]).then(res=>res);
		let data = new FormData();
		if(name!='')data.append('name',name); else return false;
		if(description!='')data.append('description',description); else return false;
		console.log('tut');
		data.append('spot_id',spot_id);
		data.append('category',category);
		data.append('item_type',0);
		data.append('method',"POST");
		if(file[0])data.append('picture',file[0]); else return false;
		let buffer = await fetch('/api/items',{method:"POST",body:data,credentials:'include'}).then(res=>res.json()).catch(err=>err);
		console.log(buffer);
		let div = document.createElement('div');
  		div.classList.add('info');
  		div.innerHTML = `	<div class="header">
								<div class="del" data-id="${buffer.id}"></div>
								<h3>${name}</h3>
							</div>
							<div class="info_image">
									<img src="${img}" />
							</div>
							<div class="content">
								<p>${description}</p>
							</div>`;
		document.querySelector('.list_content').appendChild(div);
		document.querySelector(".popup").style.display = 'none';
		document.querySelector(".subscripePopUp").style.display = 'none';	
	},
	async achievementsDelete(self){
		let data = {
			method:"DELETE",
			id:self.getAttribute('data-id')
		};
		let buffer = await fetch('/api/achievements',{method:"POST",body:JSON.stringify(data),credentials:'include',headers:{'content-type':'application/json'}}).then(res=>res.json()).catch(err=>err);
		self.closest('.info').remove();
		console.log(buffer);
	},
	async skillsDelete(self){
		let data = {
			method:"DELETE",
			id:self.getAttribute('data-id')
		};
		let buffer = await fetch('/api/skills',{method:"POST",body:JSON.stringify(data),credentials:'include',headers:{'content-type':'application/json'}}).then(res=>res.json()).catch(err=>err);
		self.closest('.info').remove();
		console.log(buffer);
	},
	async itemsDelete(self){
		let data = {
			method:"DELETE",
			id:self.getAttribute('data-id')
		};
		let buffer = await fetch('/api/items',{method:"POST",body:JSON.stringify(data),credentials:'include',headers:{'content-type':'application/json'}}).then(res=>res.json()).catch(err=>err);
		self.closest('.info').remove();
		console.log(buffer);
	}
}