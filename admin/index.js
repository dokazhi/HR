const express = require("express");
const router = express.Router();
var logger = require("morgan");

var home = require('./routes/home'); 
var chat = require('./routes/chat'); 
var hero = require('./routes/hero'); 
var cooperators = require('./routes/cooperators'); 
var dispatch = require('./routes/dispatch'); 
var baseInformation = require('./routes/baseInformation'); 
var administration = require('./routes/administration'); 
var cookieParser = require('cookie-parser');
var subd = require("../models/subd");

router.use(cookieParser("ebala"));
async function proverkaCookie(req,res,next){
	if(req.signedCookies['user_id']){
		let con = await subd.connect();
		let buffer = await con.query(`SELECT id user_id,company_id FROM user WHERE id = ?`,[req.signedCookies['user_id']]).then(res=>res[0][0]).catch(err=>err.sqlMessage);
		con.end();
		if(buffer){
			if(buffer.company_id==0){next();
			}else{res.redirect('/account');return false;}
		}
	}else{
		res.redirect('/');
	}
}
router.use(proverkaCookie)

// router.use(logger());
router.get("/",(req,res)=>{console.log("tut");res.render("../admin/views/index");});


router.use("/home"  ,home);
router.use("/chat"  ,chat);
router.use("/hero"  ,hero);
router.use("/cooperators"  ,cooperators);
router.use("/dispatch"  ,dispatch);
router.use("/baseInformation"  ,baseInformation);
router.use("/administration"  ,administration);

module.exports = router;