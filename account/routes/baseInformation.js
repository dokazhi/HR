var express = require('express');
var router = express.Router();
var subd = require('../../models/subd');

router.post("/",async (req,res)=>{
	collection={};
	let con = await subd.connect();
	let user_id = req.signedCookies['user_id'];
	let get_company = (await con.query(`
	SELECT company_id from user where
	id = ?
	`,[
		user_id
	]).catch(err=>{
		console.log(err.sqlMessage);
		return false;
	}));
	if(get_company!=false){
		
		let spots = (await con.query(`SELECT * FROM spot WHERE user_id =?`,[get_company[0][0].company_id])
		.catch(err=>{console.log(err.sqlMessage);return false;}));
		if(spots!=false){
			collection.spots=spots[0];
			if(spots[0].length>0){
			let achievements = (await con.query(`SELECT * FROM achievements	WHERE user_id = ? and category_id =?`,[get_company[0][0].company_id,spots[0][0].id])
				.catch(err=>{console.log(err.sqlMessage);return false;}));
				if(achievements!=false){collection.achievements=achievements[0];}else{collection.achievements=[];}
			}else{
				collection.achievements=[]
			}
		}else{collection.spots=[];}

		
		
		

	}else{collection.ERROR="Have no company";}
	
	con.end();


	res.render("../account/views/baseInformation",collection);
})

module.exports = router;
