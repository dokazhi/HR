var express = require('express');
var router = express.Router();
var subd = require('../../models/subd');
var request = require("request-promise")

router.post("/", async (req,res)=>{
	console.log("tuttttttt");
	console.log(req.signedCookies['user_id']);
	let result = {},user_id = req.signedCookies['user_id'];
	let con = await subd.connect();
	result['get_departments'] = await con.query(` 	SELECT 
														d.id,
														d.name 
													FROM department d
														LEFT JOIN user c
															ON c.id = ?
													WHERE 
														d.user_id = c.company_id`,[req.signedCookies['user_id']]).then(res=>res[0]).catch(err=>err.sqlMessage);
	result['achievs'] = await con.query(`	SELECT
												a.id, 
												a.name 
											FROM 
												achievements as a
											INNER JOIN 
												user_achievements as ua 
											ON 
												ua.achievement_id = a.id
											INNER JOIN 
												spot as s 
											ON 
												a.category_id = s.id
											GROUP BY 
												a.id
									`,[user_id]).then(res=>res[0]).catch(err=>err.sqlMessage);
	result['skills'] = await con.query(`SELECT
											s.id,
											s.name
										FROM
											user_skills as us
										INNER JOIN
											skills as s 
										ON
											s.id=us.skill_id
										INNER JOIN
											spot as spot 
										ON
											spot.id = s.spot_id
										GROUP BY
											s.id
									`,[user_id]).then(res=>res[0]).catch(err=>err.sqlMessage);
	result['get_users'] = await con.query(`	SELECT 
												ud.data, 
												u.id, 
												u.username, 
												u.spot_id,
												ba.path as avatar_path,
												uba.equiped as equiped
											FROM user as u
												LEFT JOIN user as us 
													ON 
														us.id = ?
												LEFT JOIN user_data as ud 
													ON 
														ud.user_id = u.id
												LEFT JOIN user_ba as uba 
													ON  
														uba.user_id = ? AND 
														uba.equiped = 1
												LEFT JOIN background_avatar  ba
													ON  
														ba.id = uba.id_ba AND 
														ba.type = 1
											WHERE 
												u.company_id = us.company_id

										`,[
											req.signedCookies['user_id'],
											req.signedCookies['user_id']
										]).then(res=>res[0]).catch(err=>err.sqlMessage);
	con.end();
	console.log(result['skills']);
	res.render("../account/views/cooperators",result);
})

module.exports = router;
