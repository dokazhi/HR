baseInformation = {
	popup(){
		document.querySelector('.popup').style.width = "700px";
		document.querySelector('.popup').style.height = "500px";
		document.querySelector('.popup').innerHTML = `
			<div class="content">
				<p><input type="text" placeholder="Название"></p>
				<textarea placeholder="Краткое описание"></textarea>
				<div class="button">
					<div class="deleteButton insert">ок</div>
				</div>
				
			</div>
		`;
		document.querySelector(".popup").style.display = 'block';
		document.querySelector(".subscripePopUp").style.display = 'block';
	},
	async clickCheck(self){
		self.closest(".blockBaseInformation").querySelector('p.active').classList.remove("active");
		self.classList.add("active");
		let couple = document.querySelectorAll(".blockBaseInformation p.active");
		// console.log(couple[0].getAttribute('id')+"CAEGORY");
		// console.log(couple[1].getAttribute('id')+"THIS IS SPOT")
		let data,head,list;
		switch(couple[0].getAttribute('id')){
			case "1":
			
				data = {
					method:"GETBYSPOT",
					spot_id:couple[1].getAttribute('id')
				}
				let achieves = await fetch("/api/achievements",{
					credentials: "include",
					method:"POST",
					headers:{
						"content-type":"application/json"
					},
					body:JSON.stringify(data)})
				.then(res=>res.json());
				console.log(achieves);
				head = document.getElementsByClassName("centerBlockBaseInformation")[0];
				
				head.childNodes[1].innerHTML = `<h1>Достижения</h1>`
				list = document.getElementsByClassName("list_content")[0];
				list.innerHTML="";
				for(let i =0;i<achieves.length;i++){
					let ach = document.createElement("div");
					ach.innerHTML=`
						<div class="header">
							<h3>${achieves[0].name}</h3>
						</div>
						<div class="info_image">
							<img src="/public${achieves[0].path}" />
						</div>
						<div class="content">
							<p>${achieves[0].description}</p>
						</div>
					`
					ach.classList.add("info");
					list.appendChild(ach);
					
				}
				// self.closest(".blockBaseInformation").querySelector('p.active')
			break;
			case "2":
			data = {
				method:"GET_SPOT",
				spot_id:couple[1].getAttribute('id')
			}
			let skills = await fetch("/api/skills",{
				credentials: "include",
				method:"POST",
				headers:{
					"content-type":"application/json"
				},
				body:JSON.stringify(data)})
			.then(res=>res.json());
			console.log(skills);
			head = document.getElementsByClassName("centerBlockBaseInformation")[0];
			
			head.childNodes[1].innerHTML = `<h1>Навыки</h1>`
			list = document.getElementsByClassName("list_content")[0];
			list.innerHTML="";
			for(let i =0;i<skills.length;i++){
				let ach = document.createElement("div");
				ach.innerHTML=`
					<div class="header">
						<h3>${skills[0].name}</h3>
					</div>
					<div class="content">
						<p>${skills[0].description}</p>
					</div>
				`
				ach.classList.add("info");
				list.appendChild(ach);
				
			}
			break;
			case "3":
			data = {
				method:"GETBYSPOT",
				spot_id:couple[1].getAttribute('id')
			}
			let items = await fetch("/api/items",{
				credentials: "include",
				method:"POST",
				headers:{
					"content-type":"application/json"
				},
				body:JSON.stringify(data)})
			.then(res=>res.json());
			
			head = document.getElementsByClassName("centerBlockBaseInformation")[0];
			
			head.childNodes[1].innerHTML = `<h1>Предметы</h1>`
			list = document.getElementsByClassName("list_content")[0];
			list.innerHTML="";
			for(let i =0;i<items.length;i++){
				let ach = document.createElement("div");
				ach.innerHTML=`
					<div class="header">
						<h3>${items[0].name}</h3>
					</div>
					<div class="info_image">
						<img src="/public${items[0].img_path}" />
					</div>
					<div class="content">
						<p>${items[0].description}</p>
					</div>
				`
				ach.classList.add("info");
				list.appendChild(ach);
				
			}
			break;
			
		}
		// for (var i = 0; i < document.querySelectorAll(".blockBaseInformation p.active").length; i++) {
		// 	console.log(document.querySelectorAll(".blockBaseInformation p.active")[i].getAttribute('id'));
		// }
	},
	delete(self){
		console.log(self.closest(".info"));
	},
	add(){
		let name = document.querySelector(".popup input").value;
		let description = document.querySelector(".popup textarea").value;
	}
}