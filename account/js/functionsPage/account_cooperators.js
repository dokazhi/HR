functions = {
	dropList(e){
		if (e.target.closest('.jDrop').nextElementSibling.style.opacity == 0) {
			e.target.closest('.jDrop').nextElementSibling.classList = 'jDrop-list';
			e.target.closest('.jDrop').lastElementChild.style.transform = 'rotate(180deg)';
			setTimeout(function(){
				e.target.closest('.jDrop').nextElementSibling.style.opacity = '1';
			},100);
		}else{
			e.target.closest('.jDrop').nextElementSibling.style.opacity = '0';
			e.target.closest('.jDrop').lastElementChild.style.transform = 'rotate(0deg)';
			setTimeout(function(){
				e.target.closest('.jDrop').closest('.jDrop').nextElementSibling.classList = 'jDrop-list hidden';
			},200);
		}
		
		
	},
	async filter(e){
		e.preventDefault();
		let dep_list = document.getElementsByClassName("filter_dep")[0].children[1];
		let dep_ids=[];
		for(let i=0;i<dep_list.childElementCount;i++){
			let cur_id =dep_list.children[i].querySelector("input").getAttribute('id').match(/\d/)[0]; 
			if(dep_list.children[i].querySelector("input").checked){
				dep_ids.push(cur_id);
			}
		}
		//achievements
		let ach_list = document.getElementsByClassName("filter_achiev")[0].children[1];
		let ach_ids = [];
		for(let i=0;i<ach_list.childElementCount;i++){
			let cur_id =ach_list.children[i].querySelector("input").getAttribute('id').match(/\d/)[0]; 
			if(ach_list.children[i].querySelector("input").checked){
				ach_ids.push(cur_id);
			}
		}
		//skills
		let skill_list = document.getElementsByClassName("filter_skill")[0].children[1];
		console.log(skill_list);
		let skill_ids = [];
		
		for(let i=0;i<skill_list.querySelectorAll("p").length;++i){
			let cur_id = skill_list.querySelectorAll("p")[i].querySelector("input").getAttribute('id').match(/\d/)[0]; 
			if(skill_list.children[i].querySelector("input").checked){
				skill_ids.push(cur_id);
			}
		}
		console.log(skill_ids);
		console.log(ach_ids);
		console.log(dep_ids);
		let data = {
			method:"FILTER",
			skill_ids:skill_ids,
			ach_ids:ach_ids,
			dep_ids:dep_ids
		}
		let filtered = await fetch("/api/users",{
			credentials: "include",
			method:"POST",
			headers:{
				"content-type":"application/json"
			},
			body:JSON.stringify(data)})
			.then(res=>res.json());
			console.log(filtered);
		document.querySelector(".contPers").innerHTML = ``;
		for (var i = 0; i < filtered.length; i++) {
			let div = document.createElement("div");
			div.className = 'jGrid-item href';
			div.setAttribute("data-page","hero");
			div.setAttribute('data-id',filtered[i].id);
			div.innerHTML = `	<div class="jGrid-item--img" style="background-image: url(/public${(filtered[i].path)?filtered[i].path:'/uploads/ba/avatar.png'})"></div>
								<div class="jGrid-item--name">
									<span class="jRound flex flex-c flex-m"><div class="jRound-item"></div></span>
									<p title="Полное имя героя">${filtered[i].data.name}</p>
								</div>`;
			document.querySelector(".contPers").appendChild(div);
		}
		if(!filtered.length)document.querySelector('.contPers').innerHTML = `<p>По вашей фильтрации нечего не найдено</p>`;
		// console.log(document.getElementsByClassName("filter_achiev")[0]);
		// console.log(document.getElementsByClassName("filter_skill")[0].children[1].childElementCount);
	},
	async search(e){
		if(e.keyCode!=13)return false;
		method: "SEARCH";
		body: search:'fdsafdsafdsa';
		let data = {
			method:"SEARCH",
			search:e.target.value
		}
		let buffer = await fetch("/api/users",{method:"POST",body:JSON.stringify(data),credentials:'include',headers:{'Content-Type':'application/json'}}).then(res=>res.json()).catch(err=>err);
		document.querySelector(".contPers").innerHTML = ``;
		for (var i = 0; i < buffer.length; i++) {
			let div = document.createElement("div");
			div.className = 'jGrid-item href';
			div.setAttribute("data-page","hero");
			div.setAttribute('data-id',buffer[i].id);
			div.innerHTML = `	<div class="jGrid-item--img" style="background-image: url(/public${(buffer[i].path)?buffer[i].path:'/uploads/ba/avatar.png'})"></div>
								<div class="jGrid-item--name">
									<span class="jRound flex flex-c flex-m"><div class="jRound-item"></div></span>
									<p title="Полное имя героя">${buffer[i].data.name}</p>
								</div>`;
			document.querySelector(".contPers").appendChild(div);
		}
		if(!filtered.length)document.querySelector('.contPers').innerHTML = `<p>Некого не найдено</p>`;
		console.log(buffer);
		console.log(e);
	}
}