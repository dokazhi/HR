
let functionNavigation = {
	clickPage: async function(self) {
		console.log("tu");
		let state = { 'page_id': self.getAttribute('data-pageId'), 'page': self.getAttribute("data-page")};
		let title = '';
		let id = "";
		(self.getAttribute("data-id"))?id = `?id=${self.getAttribute("data-id")}`:"";
		let url = `http://${location.hostname}:9901/account/#/${self.getAttribute("data-page")}${id}`;
		let data = {
			page: self.getAttribute('data-page'),
			getAttributs:dataObject.getUrlGet(url)
		};
		window.history.pushState(state, title, url);
		let html = await this.ajaxPage(data);
		this.loadingAsyncScripts(data.page,html);
	},
	inLoadingPage: async function(data) {
		let html = await this.ajaxPage(data);
		this.loadingAsyncScripts(data.page,html);
	},
	loadingAsyncScripts: function(page,html){
		let headerItem = document.querySelector(`.header-item[data-page="${page}"]`);
		let activeHeaderItem = document.createElement("span");
		let eventScript = document.createElement("script");
		let main = document.getElementsByClassName("main")[0];
		let functionScript = document.createElement("script");
		functionScript.src = `/functionsPage/account_${page}.js`;
		eventScript.src = `/events/account_${page}.js`;

		if(document.querySelector(".active"))document.querySelector(".active").querySelector('.headerItemActive').remove();		
		activeHeaderItem.className = 'headerItemActive';
		activeHeaderItem.innerHTML = `	<hr>
										<div class="circle"></div>`;
		main.innerHTML = html;
		main.appendChild(functionScript);
		main.appendChild(eventScript);
		if(document.querySelector(".active"))document.querySelector(".active").classList.remove("active");
		if(headerItem){
			headerItem.classList.add("active");
			headerItem.appendChild(activeHeaderItem);			
		}


	},
	ajaxPage:async data => {
		let buffer = await fetch(`/account/${data.page}`,{
									method:"POST",
									body:JSON.stringify({data}),
									credentials: 'include',
									headers:{'Content-Type': 'application/json',
									'Access-Control-Allow-Origin':'true'}
								}).then(res=>res.text());
		try{
			buffer = JSON.parse(buffer);
			console.log(buffer);
			if(buffer.redirect)window.location.replace(`${buffer.redirect}`);
			return false;
		}catch (err){
			return buffer;	
		}
	}
};