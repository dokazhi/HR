let dataObject = {
	data:{},
	getUrlsPage: str => {
		let url = str.match(/(\#\/)(\b[a-zA-z]+)/);
		if(!url){return '';}
		return url[2];
	},
	getUrlGet: str => {
		let gets = str.match(/\?(.*)/);
		if(!gets){return '';}
		let arr = gets[1].split("&");
		let object = {};
		for (var i = 0; i < arr.length; i++) {
			object[arr[i].split("=")[0]] = arr[i].split("=")[1];
		}
		return object;
		
	},
	getPage: function(page) {
		(!page)?page=this.getUrlsPage(location.href):"";
		this.data['getAttributs'] = this.getUrlGet(location.href);
		(page)?this.data['page'] = page:this.data['page'] = 'home';
		return this.data;
	}
}