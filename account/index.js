const express = require("express");
const router = express.Router();
var logger = require("morgan");

var home = require('./routes/home'); 
var hero = require('./routes/hero'); 
var chat = require('./routes/chat'); 
var cooperators = require('./routes/cooperators'); 
var dispatch = require('./routes/dispatch'); 
var baseInformation = require('./routes/baseInformation'); 
var cookieParser = require('cookie-parser');
var subd = require("../models/subd");

router.use(cookieParser("ebala"));
async function proverkaCookie(req,res,next){
	if(req.signedCookies['user_id']){
		let con = await subd.connect();
		let buffer = await con.query(`SELECT id user_id,company_id FROM user WHERE id = ?`,[req.signedCookies['user_id']]).then(res=>res[0][0]).catch(err=>err.sqlMessage);
		con.end();
		if(buffer){
			next();
			return false;
		}
	}
	res.redirect('/');
}
router.use(proverkaCookie);

// router.use(logger());
router.use(express.static(__dirname + '/account/js'));//js
router.use(express.static(__dirname + '/account/css'));//js
router.get("/",(req,res)=>{console.log("tut");res.render("../account/views/index");});


router.use("/home"  ,home);
router.use("/hero"  ,hero);
router.use("/chat"  ,chat);
router.use("/cooperators"  ,cooperators);
router.use("/dispatch"  ,dispatch);
router.use("/baseInformation"  ,baseInformation);

module.exports = router;